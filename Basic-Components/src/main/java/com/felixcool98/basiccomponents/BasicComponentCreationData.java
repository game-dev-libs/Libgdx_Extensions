package com.felixcool98.basiccomponents;

import com.felixcool98.components.creation.CreationData;

public class BasicComponentCreationData extends CreationData {
	private static final long serialVersionUID = 7195528522889698489L;
	
	
	public void setPosition(float x, float y) {
		put("x", x);
		put("y", y);
	}
	
	public void setRotation(float rotation) {
		put("rotation", rotation);
	}
	
	public void setSpeedXY(float xSpeed, float ySpeed) {
		put("xSpeed", xSpeed);
		put("ySpeed", ySpeed);
	}
	public void setSpeedDirection(float speed, float direction) {
		put("speed", speed);
		put("direction", direction);
	}
}
