package com.felixcool98.basiccomponents;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.felixcool98.basiccomponents.components.CenterOrigin;
import com.felixcool98.basiccomponents.components.ConnectedDrawables;
import com.felixcool98.basiccomponents.components.InstanceDrawable;
import com.felixcool98.basiccomponents.components.InstanceRotation;
import com.felixcool98.basiccomponents.components.InstanceSpeed;
import com.felixcool98.basiccomponents.components.Position;
import com.felixcool98.components.ComponentNotFoundException;
import com.felixcool98.components.creation.CreationData;
import com.felixcool98.managers.data.DataManager;
import com.felixcool98.utility.values.AdvancedDirection;

public class BasicComponentDataManager extends DataManager  {
	public BasicComponentDataManager(BasicComponentManager manager, CreationData data) {
		super(manager, data);
	}

	
	@Override
	public BasicComponentManager getComponentManager() {
		return (BasicComponentManager) super.getComponentManager();
	}
	
	
	//======================================================================
	// data getter
	//======================================================================
	
	//position
	
	public float getX() {
		if(!has(Position.class))
			throw new ComponentNotFoundException(Position.class);
		
		return get(Position.class).getX();
	}
	public float getY() {
		if(!has(Position.class))
			throw new ComponentNotFoundException(Position.class);
		
		return get(Position.class).getY();
	}
	
	//size
	
	public float getWidth() {
		return getComponentManager().getWidth();
	}
	public float getHeight() {
		return getComponentManager().getHeight();
	}
	
	//drawable
	
	public Drawable getDrawable() {
		Drawable drawable = null;
		
		if(has(ConnectedDrawables.class)) {
			drawable = getComponentManager().getDrawable(get(ConnectedDrawables.class).getSurrounding());
			
			if(drawable != null)
				return drawable;
		}
		
		if(has(InstanceDrawable.class)) {
			drawable = get(InstanceDrawable.class).getDrawable();
			
			if(drawable != null)
				return drawable;
		}
		
		return getComponentManager().getDrawable();
	}
	
	//rotation
	
	public float getRotation() {
		float rotation = getComponentManager().getRotation();
		
		if(hasInstanceRotation())
			rotation += get(InstanceRotation.class).getRotation();
		
		return rotation;
	}
	
	public boolean hasInstanceRotation() {
		return has(InstanceRotation.class);
	}
	
	//origin
	
	public float getOriginX() {
		float originX = 0;
		
		if(getComponentManager().hasComponent(CenterOrigin.class))
			originX = getWidth()/2f;
		
		return originX;
	}
	public float getOriginY() {
		float originY = 0;
		
		if(getComponentManager().hasComponent(CenterOrigin.class))
			originY = getHeight()/2f;
		
		return originY;
	}
	
	//speed
	
	public float getXSpeed() {
		if(has(InstanceSpeed.class)) {
			return get(InstanceSpeed.class).getXSpeed();
		}
		
		return 0;
	}
	public float getYSpeed() {
		if(has(InstanceSpeed.class)) {
			return get(InstanceSpeed.class).getYSpeed();
		}
		
		return 0;
	}
	
	
	//======================================================================
	// data editing
	//======================================================================
	public void accelerate(float x, float y) {
		if(!has(InstanceSpeed.class))
			throw new ComponentNotFoundException(InstanceSpeed.class);
		
		if(has(InstanceSpeed.class)) {
			get(InstanceSpeed.class).accelerate(x, y);
		}
	}
	
	public void setPosition(float x, float y) {
		if(!has(Position.class))
			throw new ComponentNotFoundException(Position.class);
		
		if(has(Position.class)) {
			get(Position.class).setPosition(x, y);
		}
	}
	public void addPosition(float x, float y) {
		if(!has(Position.class))
			throw new ComponentNotFoundException(Position.class);
		
		if(has(Position.class)) {
			get(Position.class).addPosition(x, y);
		}
	}
	
	
	//drawables
	
	public void updateSurrounding(AdvancedDirection direction) {
		get(ConnectedDrawables.class).setSurrounding(direction);
	}
	
	
	//======================================================================
	// checks
	//======================================================================
	
	//speed
	
	public boolean isMoving() {
		if(has(InstanceSpeed.class)) {
			return get(InstanceSpeed.class).getYSpeed() != 0 || get(InstanceSpeed.class).getXSpeed() != 0;
		}
		
		return false;
	}
	public boolean canMove() {
		if(has(InstanceSpeed.class)) {
			return true;
		}
		
		return false;
	}
	
	//drawables
	
	public boolean hasConnectedDrawables() {
		return has(ConnectedDrawables.class);
	}
}
