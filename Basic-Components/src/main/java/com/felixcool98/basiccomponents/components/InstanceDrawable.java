package com.felixcool98.basiccomponents.components;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.felixcool98.basiccomponents.data.DrawableData;
import com.felixcool98.components.HasData;
import com.felixcool98.components.creation.CreationData;
import com.felixcool98.managers.ComponentManager;

/**
 * 
 * CreationData needs to contain: <br>
 * drawable : Drawable<br>
 * 
 * @author felixcool98
 *
 */
public class InstanceDrawable implements HasData.CanCreateData<DrawableData> {

	@Override
	public DrawableData create(ComponentManager manager, CreationData data) {
		DrawableData drawable = new DrawableData();
		
		if(data.containsKey("drawable"))
			drawable.setDrawable((Drawable) data.get("drawable"));
		
		return drawable;
	}

}
