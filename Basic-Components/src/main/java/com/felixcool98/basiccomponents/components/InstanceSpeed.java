package com.felixcool98.basiccomponents.components;

import com.felixcool98.basiccomponents.data.SpeedData;
import com.felixcool98.components.HasData;
import com.felixcool98.components.creation.CreationData;
import com.felixcool98.managers.ComponentManager;

/**
 * 
 * CreationData needs to contain either: <br>
 * direction : float<br>
 * speed : float<br>
 * <br>
 * or:
 * xSpeed : float<br>
 * ySpeed : float<br>
 * 
 * @author felixcool98
 *
 */
public class InstanceSpeed implements HasData.CanCreateData<SpeedData> {

	@Override
	public SpeedData create(ComponentManager manager, CreationData data) {
		SpeedData speed = new SpeedData();
		
		if(data.containsKey("speed") && data.containsKey("direction")) {
			speed.setDirectionSpeed(data.getFloat("speed"), data.getFloat("direction"));
		}else if(data.containsKey("xSpeed") && data.containsKey("ySpeed")) {
			speed.setXYSpeed(data.getFloat("xSpeed"), data.getFloat("ySpeed"));
		}
		
		return speed;
	}

}
