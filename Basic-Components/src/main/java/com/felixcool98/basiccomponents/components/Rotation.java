package com.felixcool98.basiccomponents.components;

public class Rotation {
	private float rotation;
	
	
	public Rotation() {
		this(0);
	}
	public Rotation(float rotation) {
		this.rotation = rotation;
	}
	
	
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}
	
	
	public float getRotation() {
		return rotation;
	}
}
