package com.felixcool98.basiccomponents.components;

import com.badlogic.gdx.utils.XmlReader.Element;
import com.felixcool98.gdxutility.saveload.IXMLLoader;
import com.felixcool98.gdxutility.saveload.XMLLoader;
import com.felixcool98.gdxutility.saveload.XMLSaveable;
import com.felixcool98.sml.Attribute;
import com.felixcool98.sml.AttributeReader;
import com.felixcool98.sml.IAttributeReader;
import com.felixcool98.sml.IAttributeReader.AttributeReaderImpl;
import com.felixcool98.utility.fields.Getter;
import com.felixcool98.utility.fields.Min;
import com.felixcool98.utility.fields.Setter;

public class Size implements XMLSaveable {
	@XMLLoader
	public static IXMLLoader<Size> XML_LOADER = new SizeXMLLoader();
	
	@AttributeReader
	public static IAttributeReader<Size> SML_ATTRIBUTE_READER = new SizeAttributeReader();
	
	@Getter(methodName = "getWidth")
	@Setter(methodName = "setWidth")
	@Min(min = 0)
	private float width;
	@Getter(methodName = "getHeight")
	@Setter(methodName = "setHeight")
	@Min(min = 0)
	private float height;
	
	
	public Size() {
		this(1, 1);
	}
	public Size(float width, float height) {
		this.width = width;
		this.height = height;
	}
	
	
	public void set(float width, float height) {
		setWidth(width);
		setHeight(height);
	}
	
	public void setWidth(float width) {
		this.width = width;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	
	
	public float getWidth() {
		return width;
	}
	public float getHeight() {
		return height;
	}
	
	
	@Override
	public String toString() {
		return "Size(w:"+getWidth()+" |h:"+getHeight()+")";
	}


	@Override
	public Element createElement() {
		Element source = new Element("size", null);
		
		Element width = new Element("width", source);
		width.setText("" + getWidth());
		
		Element height = new Element("height", source);
		height.setText("" + getHeight());
		
		source.addChild(width);
		source.addChild(height);
		
		return source;
	}
	
	
	public static class SizeXMLLoader implements IXMLLoader<Size> {
		@Override
		public boolean isValid(Element element) {
			return element.getName().equals("size");
		}

		@Override
		public Size createFromElement(Element element) {
			float width = Float.parseFloat(element.get("width"));
			float height = Float.parseFloat(element.get("height"));
			
			return new Size(width, height);
		}
		
		@Override
		public Class<Size> getLoadedClass() {
			return Size.class;
		}
	}
	public static class SizeAttributeReader extends AttributeReaderImpl<Size>{
		public SizeAttributeReader() {
			super("size", "size");
		}
		
		@Override
		public Size process(Attribute attribute) {
			return new Size(attribute.getFloat("width", 1), attribute.getFloat("height", 1));
		}
		
	}
}
