package com.felixcool98.basiccomponents.data;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.felixcool98.basiccomponents.components.InstanceDrawable;
import com.felixcool98.data.Data;

public class DrawableData implements Data {
	private Drawable drawable;
	
	
	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}
	
	
	public Drawable getDrawable() {
		return drawable;
	}
	
	
	@Override
	public Class<InstanceDrawable> getDataClass() {
		return InstanceDrawable.class;
	}

}
