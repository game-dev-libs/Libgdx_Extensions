package com.felixcool98.basiccomponents.data;

import com.felixcool98.basiccomponents.components.Position;
import com.felixcool98.data.Data;

public class PositionData implements Data {
	private float x;
	private float y;
	
	
	public void setPosition(float x, float y) {
		this.x = x;
		this.y = y;
	}
	public void addPosition(float x, float y) {
		this.x += x;
		this.y += y;
	}
	
	
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	
	
	@Override
	public Class<Position> getDataClass() {
		return Position.class;
	}
}
