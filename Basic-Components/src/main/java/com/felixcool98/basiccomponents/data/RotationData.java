package com.felixcool98.basiccomponents.data;

import com.felixcool98.basiccomponents.components.InstanceRotation;
import com.felixcool98.data.Data;

public class RotationData implements Data {
	private float rotation;
	
	
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}
	
	
	public float getRotation() {
		return rotation;
	}
	
	
	@Override
	public Class<InstanceRotation> getDataClass() {
		return InstanceRotation.class;
	}
}
