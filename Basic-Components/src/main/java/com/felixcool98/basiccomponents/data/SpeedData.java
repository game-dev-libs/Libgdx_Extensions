package com.felixcool98.basiccomponents.data;

import com.badlogic.gdx.math.Vector2;
import com.felixcool98.basiccomponents.components.InstanceSpeed;
import com.felixcool98.components.HasData;
import com.felixcool98.data.Data;

public class SpeedData implements Data {
	private float xSpeed, ySpeed;
	
	
	public void setXYSpeed(float x, float y) {
		xSpeed = x;
		ySpeed = y;
	}
	public void setXSpeed(float x) {
		xSpeed = x;
	}
	public void setYSpeed(float y) {
		ySpeed = y;
	}
	
	public void setDirectionSpeed(float speed, float direction) {
		Vector2 vector = new Vector2(1, 1);
		vector.setLength(speed);
		vector.setAngle(direction);
		
		xSpeed = vector.x;
		ySpeed = vector.y;
	}
	
	
	public void accelerate(float x, float y) {
		xSpeed += x;
		ySpeed += y;
	}
	public void accelerateX(float x) {
		xSpeed += x;
	}
	public void accelerateY(float y) {
		ySpeed += y;
	}
	
	
	public float getXSpeed() {
		return xSpeed;
	}
	public float getYSpeed() {
		return ySpeed;
	}
	
	
	@Override
	public Class<? extends HasData<?>> getDataClass() {
		return InstanceSpeed.class;
	}
	
	
	@Override
	public String toString() {
		return "Speed Data(x:"+getXSpeed()+" |y:"+getYSpeed()+")";
	}
}
