package com.felixcool98.basiccomponents.gameworld;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TransformDrawable;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.basiccomponents.BasicComponentDataManager;
import com.felixcool98.basiccomponents.BasicComponentManager;
import com.felixcool98.basiccomponents.components.ConnectedDrawables;
import com.felixcool98.basiccomponents.components.DieOnCollision;
import com.felixcool98.basiccomponents.components.LiveTime;
import com.felixcool98.components.creation.CreationData;
import com.felixcool98.events.EventListener;
import com.felixcool98.gameworld.GameWorldDrawable;
import com.felixcool98.gameworld.GameWorldObject;
import com.felixcool98.gameworld.MovingGameWorldObject;
import com.felixcool98.gameworld.Step;
import com.felixcool98.gameworld.events.CollisionEvent;
import com.felixcool98.gameworld.events.CreationEvent;
import com.felixcool98.gameworld.events.DestroyEvent;

public class BasicComponentObject extends MovingGameWorldObject implements GameWorldDrawable {
	private BasicComponentDataManager manager;
	
	
	public BasicComponentObject(BasicComponentManager manager, CreationData data) {
		this(manager, data, "default", "default");
	}
	public BasicComponentObject(BasicComponentManager manager, CreationData data, String worldName, String...collisionWorlds) {
		super(null, worldName, collisionWorlds);
		
		this.manager = manager.createDataManager(data);
		
		setShape(new Rectangle(getDataManager().getX(), getDataManager().getY(),
				getDataManager().getWidth(), getDataManager().getHeight()));
	}
	
	
	public BasicComponentDataManager getDataManager() {
		return manager;
	}
	
	
	@Override
	public void draw(Batch batch) {
		if(!getDataManager().getComponentManager().hasDrawable())
			return;
		
		float x = manager.getX();
		float y = manager.getY();
		
		float width = manager.getWidth();
		float height = manager.getHeight();
		
		Drawable drawable = manager.getDrawable();
		if(drawable == null)
			return;
		
		if(drawable instanceof TransformDrawable) {
			float rotation = manager.getRotation();
			
			float originX = manager.getOriginX();
			float originY = manager.getOriginY();
			
			TransformDrawable tfdrawable = (TransformDrawable) drawable;
			tfdrawable.draw(batch, x, y, originX, originY, width, height, 1, 1, rotation);
		}else 
			drawable.draw(batch, x, y, width, height);
	}
	
	
	@Step(group = "move step", priority = 0)
	public void speedStep(float delta) {
		setSpeed(getDataManager().getXSpeed(), getDataManager().getYSpeed());
	}
	@Step(group = "after move", priority = 101)
	public void afterMove(float delta) {
		manager.setPosition(getShape().getAABB().getLeft(), getShape().getAABB().getBot());
	}
	@EventListener
	public void collision(CollisionEvent event) {
		if(getDataManager().getComponentManager().hasComponent(DieOnCollision.class)) {
			destroy();
			
			event.consume();
		}
	}
	
	@Step(group = "live time step", priority = -1)
	public void liveTimeStep(float delta) {
		if(!manager.has(LiveTime.class))
			return;
		
		getDataManager().get(LiveTime.class).update(delta);
		
		if(manager.get(LiveTime.class).isDead())
			destroy();
	}
	
	@EventListener
	public void onCreation(CreationEvent event) {
		if(!manager.hasConnectedDrawables()) 
			return;
		
		manager.updateSurrounding(getSurrounding());
		
		Rectangle bounds = new Rectangle(0, 0, 0, 0);
		bounds.set(getShape().getAABB().getLeft(), getShape().getAABB().getBot(),
				getShape().getAABB().getWidth(), getShape().getAABB().getHeight());
		bounds.move(-0.1f, -0.1f);
		bounds.setSize(bounds.getWidth()+0.2f, bounds.getHeight()+0.2f);
		
		for(GameWorldObject object : getIntersections(bounds)) {
			if(object.equals(this))
				continue;
			if(!(object instanceof BasicComponentObject))
				continue;
			
			BasicComponentObject obj = (BasicComponentObject) object;
			
			if(!obj.getDataManager().hasConnectedDrawables())
				continue;
			
			obj.getDataManager().updateSurrounding(obj.getSurrounding());
		}
	}
	
	@EventListener
	public void onDestruction(DestroyEvent event) {
		if(!manager.has(ConnectedDrawables.class))
			return;
		
		Rectangle bounds = new Rectangle(0, 0, 0, 0);
		bounds.set(getShape().getAABB().getLeft(), getShape().getAABB().getBot(),
				getShape().getAABB().getWidth(), getShape().getAABB().getHeight());
		bounds.move(-0.1f, 0.1f);
		bounds.setSize(bounds.getWidth()+0.2f, bounds.getHeight()+0.2f);
		
		for(GameWorldObject object : getIntersections(bounds)) {
			if(!(object instanceof BasicComponentObject))
				continue;
			
			BasicComponentObject obj = (BasicComponentObject) object;
			
			if(!obj.getDataManager().hasConnectedDrawables())
				continue;
			
			obj.getDataManager().updateSurrounding(obj.getSurrounding());
		}
	}
}
