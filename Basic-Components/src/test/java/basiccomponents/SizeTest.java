package basiccomponents;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.basiccomponents.components.Size;
import com.felixcool98.utility.fields.ObjectData;
import com.felixcool98.utility.fields.ObjectField;
import com.felixcool98.utility.fields.ObjectField.FloatField;

public class SizeTest {
	@Test
	public void test() {
		Size size = new Size(2, 1);
		
		assertEquals(2f, size.getWidth(), 0f);
		assertEquals(1f, size.getHeight(), 0f);
	}
	@Test
	public void testFieldAnnotations() {
		Size size = new Size(2, 1);
		
		assertEquals(2f, size.getWidth(), 0f);
		assertEquals(1f, size.getHeight(), 0f);
		
		ObjectData<Size> data = new ObjectData<Size>(size);
		
		ObjectField.FloatField width = (FloatField) data.getField("width");
		ObjectField.FloatField height = (FloatField) data.getField("height");
		
		width.set(size, 3f);
		height.set(size, 5f);
		
		assertEquals(3f, size.getWidth(), 0f);
		assertEquals(5f, size.getHeight(), 0f);
		
		assertEquals(width.get(size), size.getWidth(), 0f);
		assertEquals(height.get(size), size.getHeight(), 0f);
	}
}
