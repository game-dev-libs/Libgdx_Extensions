package gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.basiccomponents.BasicComponentCreationData;
import com.felixcool98.basiccomponents.BasicComponentManager;
import com.felixcool98.basiccomponents.components.CenterOrigin;
import com.felixcool98.basiccomponents.components.ComponentDrawable;
import com.felixcool98.basiccomponents.components.ConnectedDrawables;
import com.felixcool98.basiccomponents.components.DieOnCollision;
import com.felixcool98.basiccomponents.components.InstanceSpeed;
import com.felixcool98.basiccomponents.components.Position;
import com.felixcool98.basiccomponents.gameworld.BasicComponentObject;
import com.felixcool98.game.Game;
import com.felixcool98.game.GameState;
import com.felixcool98.game.GameStateAdapter;
import com.felixcool98.game.GameStateManager;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.draw.GameWorldView;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.worlds.blocks.BlockWorld;

public class BasicComponentWorld extends GameStateAdapter {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("BasicComponentWorldTest");
		config.setResizable(false);
		config.setWindowSizeLimits(700, 500, 700, 500);
		
		new Lwjgl3Application(new Game() {
			@Override
			protected GameState registerGameStates(GameStateManager manager) {
				return new BasicComponentWorld();
			}
		}, config);
	}
	
	
	private GameWorldView view;
	private GameWorld world;
	private BasicComponentObject player;
	private Batch batch;
	private ShapeRenderer renderer;
	
	
	@Override
	public void create() {
		world = new GameWorld();
		
		world.add("blocks", new BlockWorld());
		
		view = new GameWorldView(world, new OrthographicCamera(10, 10 * GdxUtils.getAspectRatio()));
		
		BasicComponentManager blockType = new BasicComponentManager();
		blockType.setSize(1, 1);
		blockType.addComponent(new Position());
		
		ConnectedDrawables connected = new ConnectedDrawables();
		connected.put(new Texture(Gdx.files.classpath("gameworld/connected.png")));
		blockType.addComponent(connected);
		
		BasicComponentManager playerType = new BasicComponentManager();
		playerType.setSize(1, 1);
		playerType.addComponent(new Position());
		playerType.addComponent(new ComponentDrawable(Gdx.files.classpath("gameworld/lake.png")));
		playerType.addComponent(new CenterOrigin());
		playerType.addComponent(new InstanceSpeed());
		playerType.addComponent(new DieOnCollision());
		
		BasicComponentCreationData data = new BasicComponentCreationData();
		
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				data.setPosition(5+i, 5+j);
				world.create(new BasicComponentObject(blockType, data, "blocks"));
			}
		}
		
		data.setPosition(9, 5);
		world.create(new BasicComponentObject(blockType, data, "default", "default", "blocks"));
		
		data.setPosition(9, 6);
		world.create(new BasicComponentObject(blockType, data, "default", "default", "blocks"));
		
		data.setPosition(9, 7);
		world.create(new BasicComponentObject(blockType, data, "default", "default", "blocks"));
		
		data.setPosition(5, 3);
		world.create(new BasicComponentObject(blockType, data, "default", "default", "blocks"));
		
		data.setPosition(6, 3);
		world.create(new BasicComponentObject(blockType, data, "default", "default", "blocks"));
		
		data.setPosition(7, 3);
		world.create(new BasicComponentObject(blockType, data, "default", "default", "blocks"));
		
		data.setPosition(9, 3);
		world.create(new BasicComponentObject(blockType, data, "default", "default", "blocks"));
		
		data.clear();
		
		data.setPosition(10, 10);
		data.setRotation(45);
//		data.setSpeedDirection(1, 45);
		player = new BasicComponentObject(playerType, data, "default", "default", "blocks");
		world.create(player);
		
		player.addListener(view);
		
		batch = new SpriteBatch();
		
		renderer = new ShapeRenderer();
		renderer.setAutoShapeType(true);
	}
	@Override
	public void render() {
		GdxUtils.clearScreen(Color.FOREST);
		
		float acceleration = GdxUtils.getDeltaTime();
		
		if(Gdx.input.isKeyPressed(Keys.W)) {
			player.getDataManager().accelerate(0, acceleration);
		}
		if(Gdx.input.isKeyPressed(Keys.A)) {
			player.getDataManager().accelerate(-acceleration, 0);
		}
		if(Gdx.input.isKeyPressed(Keys.S)) {
			player.getDataManager().accelerate(0, -acceleration);
		}
		if(Gdx.input.isKeyPressed(Keys.D)) {
			player.getDataManager().accelerate(acceleration, 0);
		}
		
		world.step(GdxUtils.getDeltaTime());
		
		batch.begin();
		view.draw(batch);
		batch.end();
		
		renderer.setColor(Color.RED);
		
		renderer.begin();
		view.debugDraw(renderer);
		view.drawDebugCamera(renderer);
		
		renderer.end();
	}
}
