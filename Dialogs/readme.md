easiest way to use is adding
**maven { url "https://jitpack.io" }** to the repositories task and
**implementation 'com.gitlab.felixcool98.Libgdx_Extensions:Dialogs:-SNAPSHOT'** to the dependencies task  
to make sure you target the right version you can replace -SNAPSHOT with an tag or a commit

contains an implementation for a dialog system  

Simple example for an dialog with 2 branches
```java
Dialog dialog = new Dialog(player, npc, "Are Dialogs working?");
dialog.speakRight();
Dialog optionYes = new Dialog(player, npc, "They work really well.");
Dialog optionNo = new Dialog(player, npc, "But how can you see this message then?");
dialog.addOption(new OptionOpenDialog("Yes", optionYes));
dialog.addOption(new OptionOpenDialog("No", optionNo));
```

To display them you can use a DialogDisplay and call DefaultValues.load() before otherwise it can crash.
```java
DefaultValues.load();

DialogDisplayStyle style = new DialogDisplayStyle();{
	style.textHeight = 300;
	style.textWidth = 600;
	
	style.characterDisplayStyle = new CharacterDisplayStyle();{
		style.characterDisplayStyle.width = 64;
		style.characterDisplayStyle.imageHeight = 64;
	}
}

DialogDisplay display = new DialogDisplay(style);

display.setDialog(dialog);

stage.addActor(display);
```

The display style is used to define the size of the dialog