package com.felixcool98.dialogs.gui;

import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.felixcool98.dialogs.Character;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisImage;
import com.kotcrab.vis.ui.widget.VisLabel;

public class CharacterDisplay extends Table {
	private VisImage image;
	private VisLabel label;
	
	private float imageHeight;
	private float textHeight;
	
	private boolean needsRebuild = true;
	
	
	public CharacterDisplay(CharacterDisplayStyle style) {
		this();
		
		setStyle(style);
	}
	public CharacterDisplay() {
		this.image = new VisImage();
		
		this.label = new VisLabel();
	}
	
	
	public void rebuild() {
		this.clearChildren();
		
		this.add(image).width(getWidth()).height(imageHeight);
		
		this.row();
		
		this.add(label).width(getWidth()).height(textHeight);

		needsRebuild = false;
		
		pack();
	}
	
	
	public void setImageHeight(float height) {
		this.imageHeight = height;
		
		invalidate();
		
		needsRebuild = true;
	}
	public void setTextHeight(float height) {
		this.textHeight = height;
		
		invalidate();
		
		needsRebuild = true;
	}
	
	
	public void setStyle(CharacterDisplayStyle style) {
		setWidth(style.width);
		setImageHeight(style.imageHeight);
		setTextHeight(style.textHeight);
		
		rebuild();
	}
	
	
	public void setCharacter(Character character, String image) {
		if(character == null) {
			label.setColor(0, 0, 0, 0);
			this.image.setColor(0, 0, 0, 0);
			
			return;
		}
		
		
		label.setText(character.getName());
		if(character.getImage(image) != null)
			this.image.setDrawable(new TextureRegionDrawable(character.getImage(image)));
		else
			this.image.setDrawable((TextureRegionDrawable) null);
	}
	
	
	@Override
	public void setColor(float r, float g, float b, float a) {
		super.setColor(r, g, b, a);
		
		image.setColor(r, g, b, a);
		label.setColor(r, g, b, a);
	}
	@Override
	public void validate() {
		super.validate();

		if(needsRebuild)
			rebuild();
	}
	@Override
	public void setWidth(float width) {
		super.setWidth(width);
		
		invalidate();
		
		needsRebuild = true;
	}
	
	
	public static class CharacterDisplayStyle {
		public float width;
		public float imageHeight;
		
		public float textHeight = VisUI.getSkin().get(LabelStyle.class).font.getLineHeight();
	}
}
