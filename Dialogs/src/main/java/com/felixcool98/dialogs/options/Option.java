package com.felixcool98.dialogs.options;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.dialogs.gui.DialogDisplay;

public abstract class Option {
	private String text;
	
	private boolean hideIfCantbeClicked = false;
	
	private List<OptionChosenListener> listeners = new LinkedList<>();
	
	
	public Option(String text) {
		this.text = text;
	}
	
	
	public String getText() {
		return text;
	}
	
	
	public void hideIfOptionCantBeClicked(boolean value) {
		this.hideIfCantbeClicked = value;
	}
	
	
	/**
	 * will be called if the button representing this option gets clicked
	 * 
	 * @param display the display this option is shown in can be used to call more dialogs
	 */
	public abstract void clicked(DialogDisplay display);
	public boolean canBeClicked() {
		return true;
	};
	
	public boolean hidden() {
		if(canBeClicked())
			return false;
		
		return hideIfCantbeClicked;
	}
	
	
	public void addListener(OptionChosenListener listener) {
		listeners.add(listener);
	}
	public void removeListener(OptionChosenListener listener) {
		listeners.remove(listener);
	}
	
	public void callListeners() {
		for(OptionChosenListener listener : listeners) {
			listener.chosen();
		}
	}
	
	
	public static interface OptionChosenListener {
		public void chosen();
	}
}
