package com.felixcool98.dialogs.res;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.felixcool98.gdxutility.textures.TextureManager;

public class DefaultValues {
	public static TextureRegion DEFAULT_CHARACTER_IMAGE;
	
	
	public static void load() {
		DEFAULT_CHARACTER_IMAGE = TextureManager.INSTANCE.getTextureRegion(Gdx.files.classpath("com/felixcool98/dialogs/res/DefaultChar.png"));
	}
}
