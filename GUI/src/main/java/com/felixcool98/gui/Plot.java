package com.felixcool98.gui;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;

public class Plot extends Widget {
	private ShapeRenderer renderer = new ShapeRenderer();
	
	private List<float[]> points = new LinkedList<>();
	
	private boolean needsRecalculating = true;
	
	private FrameBuffer fbo;
	private TextureRegion fboTexture;
	
	private float drawScaleX = 1;
	private float drawScaleY = 1;
	
	private float drawOffSetX;
	private float drawOffSetY;
	
	private float zoom = 1;
	
	
	public Plot() {
		renderer.setAutoShapeType(true);
		setSize(1, 1);
		setTouchable(Touchable.enabled);
	}
	
	
	//======================================================================
	// auto scaleing
	//======================================================================
	private void calculateDrawStuff() {
		renderToFBO();
	}
	private void renderToFBO() {
		if(fbo != null)
			fbo.dispose();
		
		if(points.isEmpty())
			return;
		
		float maxX = -Float.MAX_VALUE;
		float maxY = -Float.MAX_VALUE;
		
		float minX = Float.MAX_VALUE;
		float minY = Float.MAX_VALUE;
		
		for(float[] point : points) {
			float x = point[0];
			float y = point[1];
			
			if(x > maxX)
				maxX = x;
			if(y > maxY)
				maxY = y;
			
			if(x < minX)
				minX = x;
			if(y < minY)
				minY = y;
		}
		
		
		float drawOffSetX = -minX;
		float drawOffSetY = -minY;
		
		float width = (float) Math.ceil(maxX - minX);
		float height = (float)Math.ceil(maxY - minY);
		
		float factor = 5;
		float lineWidth = 2;
		
		int fboWidth = (int) (Math.ceil((points.size()*factor)));
		int fboHeight = (int) (Math.ceil((points.size()*factor)));
		
		fbo = new FrameBuffer(Format.RGBA8888, fboWidth, fboHeight, false);
		
		float xScale = fbo.getWidth()/width;
		float yScale = fbo.getHeight()/height;
		
		fbo.begin();{
			Gdx.gl.glClearColor(0, 0, 0, 0);
			
			Matrix4 matrix = new Matrix4();
			matrix.setToOrtho2D(0, 0, fbo.getWidth(), fbo.getHeight());
			renderer.setProjectionMatrix(matrix);
			Gdx.gl.glLineWidth(lineWidth);
			
			renderer.begin();{
				for(int i = 0; i < points.size()-1; i++) {
					float x1 = points.get(i)[0];
					float y1 = points.get(i)[1];
					
					float x2 = points.get(i+1)[0];
					float y2 = points.get(i+1)[1];
					
					float finalX1 = (x1+drawOffSetX)*xScale;
					float finalY1 = (y1+drawOffSetY)*yScale;
					
					float finalX2 = (x2+drawOffSetX)*xScale;
					float finalY2 = (y2+drawOffSetY)*yScale;
					
					renderer.line(finalX1, finalY1, finalX2, finalY2);
				}
			}
			renderer.end();
		}fbo.end();
		
		Gdx.gl.glLineWidth(1);
		
		fbo.getColorBufferTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		fboTexture = new TextureRegion(fbo.getColorBufferTexture());
		fboTexture.flip(false, true);
	}
	@Override
	public void validate() {
		super.validate();
		
		if(needsRecalculating) {
			calculateDrawStuff();
			
			needsRecalculating = false;
		}
	}
	
	
	//======================================================================
	// adding points
	//======================================================================
	public void clearPoints() {
		this.points.clear();
		
		newPointsAdded();
	}
	
	
	public void addPoints(List<float[]> points) {
		this.points.addAll(points);
		
		newPointsAdded();
	}
	public void addPoints(float... points) {
		if(points.length%2 == 1) 
			throw new IllegalArgumentException("points must consist of a even number of floats");
		
		for(int i = 0; i < points.length; i+=2) {
			this.points.add(new float[] {points[i], points[i+1]});
		}
		
		newPointsAdded();
	}
	public void addPoints(float[]... points) {
		for(float[] point : points) {
			this.points.add(point);
		}
		
		newPointsAdded();
	}
	private void newPointsAdded() {
		sortList();
		needsRecalculating = true;
	}
	private void sortList() {
		points.sort(new Comparator<float[]>() {
			@Override
			public int compare(float[] arg0, float[] arg1) {
				float x1 = arg0[0];
				float y1 = arg0[1];
				
				float x2 = arg1[0];
				float y2 = arg1[1];
				
				if(x1 < x2)
					return -1;
				if(x1 > x2)
					return 1;
				if(y1 < y2)
					return -1;
				if(y1 > y2)
					return 1;
				
				return 0;
			}
		});
	}
	
	
	//======================================================================
	// drawing
	//======================================================================
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
		if(fbo == null)
			return;
		
		Rectangle scissors = new Rectangle();
		Rectangle clipBounds = new Rectangle(getX(), getY(), getWidth(), getHeight());
		ScissorStack.calculateScissors(getStage().getCamera(), batch.getTransformMatrix(), clipBounds, scissors);
		
		batch.flush();
		ScissorStack.pushScissors(scissors);
		float xScale = getWidth()/fboTexture.getRegionWidth()*drawScaleX;
		float yScale = getHeight()/fboTexture.getRegionHeight()*drawScaleY;
		
		float srcX = fboTexture.getRegionWidth()/2f-drawOffSetX;
		float srcY = fboTexture.getRegionHeight()/2f-drawOffSetY;
		
		float x = getX()+drawOffSetX*drawScaleX-srcX+getWidth()/2f;
		float y = getY()+drawOffSetY*drawScaleY-srcY+getHeight()/2f;
		
		batch.draw(fboTexture,
				x, y,
				srcX, srcY,
				fboTexture.getRegionWidth(), fboTexture.getRegionHeight(),
				xScale, yScale,
				0);
		
		batch.flush();
		ScissorStack.popScissors();
	}
	
	//zoom
	public void zoom(float zoom) {
		if(this.zoom*zoom < 1) {
			multiplyDrawScale(1/this.zoom);
			this.zoom = 1;
			this.drawOffSetX = 0;
			this.drawOffSetY = 0;
			return;
		}
		
		this.zoom *= zoom;
		
		multiplyDrawScale(zoom);
	}
	
	//setter
	public void setDrawScale(float scale) {
		drawScaleX = scale;
		drawScaleY = scale;
	}
	public void multiplyDrawScale(float scale) {
		multiplyDrawScaleX(scale);
		multiplyDrawScaleY(scale);
	}
	
	//drawScaleX
	public void multiplyDrawScaleX(float scale) {
		drawScaleX *= scale;
	}
	
	//drawScaleY
	public void multiplyDrawScaleY(float scale) {
		drawScaleY *= scale;
	}
	
	//drawOffsetX
	public void setDrawOffSetX(float x) {
		drawOffSetX = x;
	}
	public void addToDrawOffSetX(float x) {
		drawOffSetX += x;
	}
	
	//drawOffsetY
	public void setDrawOffSetY(float y) {
		drawOffSetY = y;
	}
	public void addToDrawOffSetY(float y) {
		drawOffSetY += y;
	}
	
	
	//getter
	
	//drawOffSetX
	public float getDrawOffSetX() {
		return drawOffSetX;
	}
	
	//drawOffSetY
	public float getDrawOffSetY() {
		return drawOffSetY;
	}
	
	//drawScaleX
	public float getDrawScaleX() {
		return drawScaleX;
	}
	
	//drawScaleY
	public float getDrawScaleY() {
		return drawScaleY;
	}
	
	
	public void addDragListener() {
		addListener(new DragListener() {
			private float x, y;
			private float oldX, oldY;
			@Override
			public void dragStart(InputEvent event, float x, float y, int pointer) {
				this.x = x;
				this.y = y;
				
				oldX = Plot.this.getDrawOffSetX();
				oldY = Plot.this.getDrawOffSetY();
			}
			@Override
			public void drag(InputEvent event, float x, float y, int pointer) {
				Plot.this.setDrawOffSetX(oldX+(-(this.x-x)/Plot.this.getDrawScaleX()));
				Plot.this.setDrawOffSetY(oldY+(-(this.y-y)/Plot.this.getDrawScaleY()));
			}
		});
	}
	public void addZoomListener(float rate) {
		addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Plot.this.getStage().setScrollFocus(Plot.this);
				return super.touchDown(event, x, y, pointer, button);
			}
			@Override
			public boolean scrolled(InputEvent event, float x, float y, int amount) {
				if(amount < 0)
					Plot.this.zoom(rate);
				else if(amount > 0)
					Plot.this.zoom(1/rate);
				
				return super.scrolled(event, x, y, amount);
			}
		});
	}
}
