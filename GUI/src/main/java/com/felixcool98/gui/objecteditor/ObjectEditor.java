package com.felixcool98.gui.objecteditor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.felixcool98.utility.fields.ObjectData;
import com.felixcool98.utility.fields.ObjectField;
import com.felixcool98.utility.fields.ObjectField.BooleanField;
import com.felixcool98.utility.fields.ObjectField.FloatField;
import com.felixcool98.utility.fields.ObjectField.IntegerField;
import com.felixcool98.utility.fields.ObjectField.StringField;
import com.felixcool98.widgets.html.HTMLTableBuilder;
import com.kotcrab.vis.ui.widget.Tooltip;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.kotcrab.vis.ui.widget.VisTree;

public class ObjectEditor extends VisTree {
	private Object object;
	
	
	public ObjectEditor() {
		
	}
	
	
	public void setObject(Object object) {
		this.object = object;
		
		clearChildren();
		
		try {
			if(object.getClass().getConstructor() == null)
				return;
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		
		ObjectData<?> data = new ObjectData<>(object);
		
		for(ObjectField<?> field : data.getFields()) {
			if(field instanceof StringField) {
				addStringField((StringField) field);
			}else if(field instanceof FloatField) {
				addFloatField((FloatField) field);
			}else if(field instanceof IntegerField) {
				addIntegerField((IntegerField) field);
			}else if(field instanceof BooleanField) {
				addBooleanField((BooleanField) field);
			}
		}
	}
	
	private void addStringField(StringField field) {
		VisLabel nameLabel = new VisLabel(field.getName());
		VisLabel stateLabel = new VisLabel("" + field.get(object));
		
		Table table = new Table();{
			table.add(nameLabel).padRight(10);
			table.add(stateLabel);
		}Node node = new Node(table);
		
		Table editor = new Table();{
			VisLabel validLabel = new VisLabel();
			validLabel.setColor(Color.RED);
			
			VisTextField textField = new VisTextField(""+field.get(object));
			table.add(textField);
			
			VisTextButton update = new VisTextButton("update");{
				update.addListener(new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						if(!field.valid(textField.getText())) {
							validLabel.setText(field.getInvalidText(textField.getText()));
							return;
						}
						
						validLabel.setText("");
						
						field.set(object, textField.getText());
						stateLabel.setText(textField.getText());
					}
				});
			}
			
			editor.add(textField);
			editor.add(update);
			editor.add(validLabel);
		}node.add(new Node(editor));
		
		if(field.hasMin()) {
			node.add(new Node(new VisLabel("minimum: " + Math.round(field.getMin()) + " characters")));
		}
		if(field.hasMax()) {
			node.add(new Node(new VisLabel("maximum: " + Math.round(field.getMax()) + " characters")));
		}
		
		add(node);
	}
	private void addFloatField(FloatField field) {
		VisLabel nameLabel = new VisLabel(field.getName());
		VisLabel stateLabel = new VisLabel("" + field.get(object));
		
		Table table = new Table();{
			table.add(nameLabel).padRight(10);
			table.add(stateLabel);
			
			if(field.hasDescription()) {
				new Tooltip.Builder(new HTMLTableBuilder(field.getDescription()).build()).target(table).build();
			}
		}Node node = new Node(table);
		
		Table editor = new Table();{
			VisLabel validLabel = new VisLabel();
			validLabel.setColor(Color.RED);
			
			VisTextField textField = new VisTextField(""+field.get(object));
			table.add(textField);
			
			VisTextButton update = new VisTextButton("update");{
				update.addListener(new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						try {
							Float.parseFloat(textField.getText());
						} catch (Exception e) {
							error();
							
							return;
						}
						
						if(!field.valid(Float.parseFloat(textField.getText()))) {
							if(field.hasMax()) {
								field.set(object, field.getMax());
							}
							if(field.hasMin()) {
								field.set(object, field.getMin());
							}
							
							error();
							
							return;
						}
						
						validLabel.setText("");
						
						field.set(object, Float.parseFloat(textField.getText()));
						stateLabel.setText(textField.getText());
					}
					private void error() {
						try {
							validLabel.setText(field.getInvalidText(Float.parseFloat(textField.getText())));
						} catch (NumberFormatException e) {
							validLabel.setText("no number");
						}
					}
				});
			}
			
			editor.add(textField);
			editor.add(update);
			editor.add(validLabel);
		}node.add(new Node(editor));
		
		if(field.hasMin()) {
			node.add(new Node(new VisLabel("minimum: " + field.getMin())));
		}
		if(field.hasMax()) {
			node.add(new Node(new VisLabel("maximum: " + field.getMax())));
		}
		
		add(node);
	}
	private void addIntegerField(IntegerField field) {
		VisLabel nameLabel = new VisLabel(field.getName());
		VisLabel stateLabel = new VisLabel("" + field.get(object));
		
		Table table = new Table();{
			table.add(nameLabel).padRight(10);
			table.add(stateLabel);
		}Node node = new Node(table);
		
		Table editor = new Table();{
			VisLabel validLabel = new VisLabel();
			validLabel.setColor(Color.RED);
			
			VisTextField textField = new VisTextField(""+field.get(object));
			table.add(textField);
			
			VisTextButton update = new VisTextButton("update");{
				update.addListener(new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						try {
							Integer.parseInt(textField.getText());
						} catch (Exception e) {
							error();
							
							return;
						}
						
						if(!field.valid(Integer.parseInt(textField.getText()))) {
							if(field.hasMax()) {
								field.set(object, (int) field.getMax());
							}
							if(field.hasMin()) {
								field.set(object, (int) field.getMin());
							}
							
							error();
							
							return;
						}
						
						validLabel.setText("");
						
						field.set(object, Integer.parseInt(textField.getText()));
						stateLabel.setText(textField.getText());
					}
					private void error() {
						try {
							validLabel.setText(field.getInvalidText(Integer.parseInt(textField.getText())));
						} catch (NumberFormatException e) {
							validLabel.setText("no number");
						}
					}
				});
			}
			
			editor.add(textField);
			editor.add(update);
			editor.add(validLabel);
		}node.add(new Node(editor));
		
		if(field.hasMin()) {
			node.add(new Node(new VisLabel("minimum: " + field.getMin())));
		}
		if(field.hasMax()) {
			node.add(new Node(new VisLabel("maximum: " + field.getMax())));
		}
		
		add(node);
	}
	private void addBooleanField(BooleanField field) {
		VisLabel nameLabel = new VisLabel(field.getName());
		VisLabel stateLabel = new VisLabel("" + field.get(object));
		
		Table table = new Table();{
			table.add(nameLabel).padRight(10);
			table.add(stateLabel);
		}Node node = new Node(table);
		
		VisTextButton button = new VisTextButton(""+ field.get(object).toString());
		button.addListener(
				new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				field.switchState(object);
				
				button.setText(""+ field.get(object).toString());
				
				stateLabel.setText("" + field.get(object));
			}
		});node.add(new Node(button));
		
		add(node);
	}
}
