package com.felixcool98.gui.texteditor;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.felixcool98.gui.texteditor.TextHighlight.HighlightElement;

public class Line extends Widget {
	static float blinkTime = 0;
	
	private BitmapFont font = new BitmapFont();
	private List<Element> elements = new LinkedList<>();
	private TextHighlight highlight;
	
	
	private String text = "";
	private int number = -1;
	private Vector2 prefSize = new Vector2();
	
	private int cursor = 0;
	
	
	public Line(TextHighlight highlight) {
		this.highlight = highlight;
		
		computePrefSize();
	}
	
	
	protected void computePrefSize() {
		float width = 0;
		
		for(Element element : elements) {
			width += element.getWidth();
		}
		
		prefSize.set(width+20, font.getLineHeight());
	}
	protected void recreateElements() {
		elements.clear();
		
		
		if(highlight == null) {
			elements.add(new Element(text, font));
			return;
		}
		
		
		String rest = text;
		String current = "";
		
		while(rest.length() > 0) {
			boolean found = false;
			
			for(HighlightElement element : highlight.getHighlights()) {
				if(rest.startsWith(element.getText())) {
					found = true;
					
					if(current.length() > 0) {
						elements.add(new Element(current, font));
						
						current = "";
					}
					
					rest = rest.replaceFirst(element.getText(), "");
					elements.add(new Element(element.getText(), font, element.getColor()));
				}
			}
			
			if(!found) {
				current += rest.substring(0, 1);
				rest = rest.substring(1);
			}
		}
		
		if(current.length() > 0) {
			elements.add(new Element(current, font));
		}
		
		if(elements.size() == 0) {
			elements.add(new Element("", font));
		}
		
		computePrefSize();
	}
	
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		float x = getX();
		
		float y = getY() + 20;
		
		if(number > 0)
			font.draw(batch, ""+number, x, y);
		
		x += 20;
		
		int o = 0;
		boolean drawn = false;
		
		for(int i = 0; i < elements.size(); i++) {
			Element element = elements.get(i);
			
			boolean drawCursor = false;
			
			if(cursor >= o && cursor < o + element.textLength() || i == elements.size()-1)
				drawCursor = true;
			if(!(getStage().getKeyboardFocus() != null && getStage().getKeyboardFocus().equals(this)))
				drawCursor = false;
			
			if(drawCursor && !drawn) {
				if(blinkTime % 1.2 < 0.6)
					element.draw(batch, x, y, cursor-o, "|");
				else
					element.draw(batch, x, y, cursor-o, " ");
				drawn = true;
			}else {
				element.draw(batch, x, y);
			}
			
			x += element.getWidth();
			o += element.textLength();
		}
	}
	
	
	//================================================================================================================================================
	// getter
	//================================================================================================================================================
	public String getText() {
		return text;
	}
	@Override
	public float getWidth() {
		return getPrefWidth();
	}
	@Override
	public float getPrefWidth() {
		return prefSize.x+4;
	}
	@Override
	public float getHeight() {
		return getPrefHeight();
	}
	@Override
	public float getPrefHeight() {
		return prefSize.y+4;
	}
	
	
	//================================================================================================================================================
	// cursor
	//================================================================================================================================================
	public void moveCursorRight() {
		if(cursor < text.length())
			cursor ++;
	}
	public void moveCursorLeft() {
		if(cursor > 0)
			cursor --;
	}
	public void setCursor(int pos) {
		cursor = pos;
		
		if(cursor > text.length())
			cursor = text.length();
	}
	public void replaceCursor(float x) {
		float xx = 20;
		
		GlyphLayout layout = new GlyphLayout();
		
		for(int i = 0; i < text.length(); i++) {
			layout.setText(font, ""+text.charAt(i));
			float width = layout.width;
			
			
			if(x < xx + width/2f) {
				cursor = i;
				
				return;
			}
			
			xx += width;
		}
		
		cursor = text.length();
	}
	public int getCursor() {
		return cursor;
	}
	
	
	//================================================================================================================================================
	// Changing text
	//================================================================================================================================================
	public void setText(String text) {
		this.text = text;
		
		recreateElements();
	}
	public void addText(String text) {
		this.text += text;
		
		recreateElements();
	}
	public void addTextAtCursor(String text) {
		addTextToPos(text, cursor);
	}
	public void addTextToPos(String text, int pos) {
		this.text = new StringBuilder().append(this.text).insert(pos, text).toString();
		
		recreateElements();
	}
	public boolean removeSymbolAtCursor() {
		if(text.length() == 0)
			return true;
		
		if(cursor <= 0)
			return false;
		
		if(cursor < text.length())
			this.text = this.text.substring(0, cursor - 1) + this.text.substring(cursor);
		else {
			this.text = this.text.substring(0, this.text.length()-1);
			setCursor(text.length());
		}
		
		recreateElements();
		
		return false;
	}
	
	
	//================================================================================================================================================
	// setter
	//================================================================================================================================================
	public void setNumber(int number) {
		this.number = number;
	}
	
	
	//================================================================================================================================================
	// getter
	//================================================================================================================================================
	public int getNumber() {
		return number;
	}
}
