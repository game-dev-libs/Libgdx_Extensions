package com.felixcool98.gui.texteditor;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisScrollPane;

public class TextEditor extends Table {
	private Table lines;
	private TextHighlight highlight;
	
	private List<Line> lineList = new LinkedList<>();
	
	private float lineWidth = 600;
	private float lineHeight = 600;
	
	
	public TextEditor(TextHighlight highlight) {
		this.highlight = highlight;
		
		init();
	}
	
	
	private void init() {
		initLineTable();
	}
	
	private void initLineTable() {
		
		
		lines = new Table();{
			addNewLine();
		}VisScrollPane pane = new VisScrollPane(lines);
		
		add(pane).size(lineWidth, lineHeight);
	}
	
	
	//================================================================================================================================================
	// adding lines
	//================================================================================================================================================
	public void addNewLine() {
		Line line = new Line(highlight);
		
		addNewLine(line);
	}
	public void addNewLine(String text) {
		Line line = new Line(highlight);
		line.setText(text);
		
		addNewLine(line);
	}
	public void addNewLine(Line line) {
		lineList.add(line);
		lines.add(line).align(Align.left).expandX().fillX().row();
		
		line.addListener(new ClickListener(Buttons.LEFT) {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getStage().setKeyboardFocus(line);
				
				line.replaceCursor(x);
			}
			@Override
			public boolean keyTyped(InputEvent event, char character) {
				if(character == '\b') {
					boolean remove = line.removeSymbolAtCursor();
					
					if(remove) {
						line.remove();
						lineList.remove(line);
						renumber();
					}
				}else {
					line.addTextAtCursor(""+character);
					line.moveCursorRight();
				}
				
				return super.keyTyped(event, character);
			}
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if(keycode == Keys.LEFT) {
					line.moveCursorLeft();
				}else if(keycode == Keys.RIGHT) {
					line.moveCursorRight();
				}else if(keycode == Keys.DOWN) {
					int n = lineList.indexOf(line);
					
					if(n + 1 >= lineList.size()) {
						addNewLine();
					}
					
					getStage().setKeyboardFocus(lineList.get(n + 1));
					lineList.get(n + 1).setCursor(line.getCursor());
				}else if(keycode == Keys.UP) {
					int n = lineList.indexOf(line);
					
					if(n != 0) {
						getStage().setKeyboardFocus(lineList.get(n - 1));
						lineList.get(n - 1).setCursor(line.getCursor());
					}
					
				}
				
				return super.keyDown(event, keycode);
			}
		});
		
		renumber();
	}
	public void addFile(FileHandle handle) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(new FileInputStream(handle.file()))));
			
			String line = reader.readLine();
			
			while(line != null) {
				addNewLine(line);
				
				line = reader.readLine();
			}
			
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private void renumber() {
		int i = 0;
		
		for(Line line : lineList) {
			line.setNumber(i);
			
			i++;
		}
	}
	
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
		Line.blinkTime += Gdx.graphics.getDeltaTime();
	}
	
	
	//================================================================================================================================================
	// getter
	//================================================================================================================================================
	public String[] getLines() {
		List<String> strings = new LinkedList<>();
		
		for(Line line : lineList) {
			strings.add(line.getText());
		}
		
		return strings.toArray(new String[0]);
	}
}
