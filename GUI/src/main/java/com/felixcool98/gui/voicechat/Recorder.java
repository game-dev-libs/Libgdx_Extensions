package com.felixcool98.gui.voicechat;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.AudioRecorder;

public class Recorder {
	private final int samples;
	private AudioRecorder recorder;
	
	private boolean running = false;
	
	private List<RecordListener> listeners = new LinkedList<>();
	private List<VoiceFunction> functions = new LinkedList<>();
	
	
	public Recorder(int samples) {
		this.samples = samples;
		recorder = Gdx.audio.newAudioRecorder(samples, true);
	}
	
	
	public void start() {
		running = true;
		
		new Thread(()-> {
			while(running) {
				short[] data = new short[samples/8];
				recorder.read(data, 0, data.length);
				data = applyFunctions(data);
				pushData(data);
			}
		}).start();
	}
	public void stop() {
		running = false;
	}
	
	
	private void pushData(final short[] data) {
		for(RecordListener listener : listeners) {
			new Thread(()-> {
				listener.recordPushed(data);
			}).start();
		}
	}
	
	
	public void dispose() {
		recorder.dispose();
	}
	
	
	//======================================================================
	// voice functions
	//======================================================================
	public void addFunction(VoiceFunction function) {
		functions.add(function);
	}
	private short[] applyFunctions(short[] data) {
		if(functions.isEmpty())
			return data;
		
		for(VoiceFunction function : functions) {
			data = function.modify(data);
		}
		
		return data;
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	public int getSamples() {
		return samples;
	}
	
	
	//======================================================================
	// listeners
	//======================================================================
	
	public void addListener(RecordListener listener) {
		listeners.add(listener);
	}
	
	
	public static interface RecordListener {
		public void recordPushed(short[] data);
		
		
		public static abstract class MeanVolume implements RecordListener{
			private int accuracy;
			
			
			public MeanVolume() {
				this(10);
			}
			public MeanVolume(int accuracy) {
				this.accuracy = accuracy;
			}
			
			
			@Override
			public void recordPushed(short[] data) {
				short lowest = Short.MAX_VALUE;
				short highest = Short.MIN_VALUE;
				
				for(int i = 0; i < data.length; i+=accuracy) {
					if(data[i] < lowest)
						lowest = data[i];
					
					if(data[i] > highest)
						highest = data[i];
				}
				
				
				meanVolume(Math.abs(highest-lowest));
			}
			public abstract void meanVolume(float volume);
		}
	}
}
