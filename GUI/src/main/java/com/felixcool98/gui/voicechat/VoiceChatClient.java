package com.felixcool98.gui.voicechat;

import java.util.LinkedList;
import java.util.List;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.felixcool98.gui.voicechat.Recorder.RecordListener;

public class VoiceChatClient extends com.felixcool98.net.Client implements RecordListener {
	private List<RecordListener> listeners = new LinkedList<>();
	
	
	public VoiceChatClient() {
		super(22050);
	}
	@Override
	public void registerClasses(Kryo kryo) {
		VoiceChatKryo.prepareKry(kryo);
	}
	@Override
	public void received(Connection connection, Object object) {
		if(object instanceof short[]) {
			for(RecordListener listener : listeners) {
				new Thread(()-> {
					listener.recordPushed((short[]) object);
				}).start();
			}
		}
	}
	
	
	public boolean isConnected() {
		return getClient().isConnected();
	}
	
	
	@Override
	public void recordPushed(short[] data) {
		getClient().sendTCP(data);
	}
	public void addListener(RecordListener listener) {
		listeners.add(listener);
	}
}
