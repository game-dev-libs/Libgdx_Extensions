package com.felixcool98.gui.voicechat;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.felixcool98.net.Server;

public class VoiceChatServer extends Server {
	public VoiceChatServer() {
		super(22050);
	}

	@Override
	public void registerClasses(Kryo kryo) {
		VoiceChatKryo.prepareKry(kryo);
	}

	@Override
	public void received(Connection connection, Object object) {
		if(object instanceof short[]) {
			getServer().sendToAllExceptTCP(connection.getID(), object);
		}
	}
	
}
