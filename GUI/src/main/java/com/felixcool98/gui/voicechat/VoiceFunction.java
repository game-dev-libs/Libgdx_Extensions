package com.felixcool98.gui.voicechat;

public interface VoiceFunction {
	public short[] modify(short[] data);
}
