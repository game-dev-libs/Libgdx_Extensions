package com.felixcool98.game;

public class GameStateAdapter extends GameState {
	@Override
	public void create() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {}

	@Override
	public void render() {}

	@Override
	public void resize(int width, int height) {}
}
