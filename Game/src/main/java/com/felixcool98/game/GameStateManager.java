package com.felixcool98.game;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.utils.Disposable;

public class GameStateManager implements Disposable {
	private Map<String, ApplicationListener> states = new HashMap<String, ApplicationListener>();
	
	private ApplicationListener current;
	private ApplicationListener next;
	
	
	public void register(String name, ApplicationListener state) {
		states.put(name, state);
	}
	
	
	public void enter(String state) {
		enter(states.get(state));
	}
	public void enter(ApplicationListener state) {
		if(state instanceof GameState)
			((GameState) state).setManager(this);
		
		next = state;
	}
	
	
	public void render() {
		switchToNext();
		
		current.render();
	}
	
	private void switchToNext() {
		if(this.next == null)
			return;
		
		ApplicationListener next = this.next;
		
		this.next = null;
		
		next.create();
		
		if(current != null) 
			current.dispose();
		
		current = next;
		
		if(this.next != next)
			switchToNext();
	}
	
	
	public void resize(int width, int height) {
		if(current != null)
			current.resize(width, height);
	}
	
	
	@Override
	public void dispose() {
		for(ApplicationListener state : states.values()) {
			state.dispose();
		}
	}
}
