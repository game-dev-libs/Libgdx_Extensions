package com.felixcool98.lighting;

import java.util.List;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.felixcool98.lighting.lightsources.Light;
import com.felixcool98.lighting.occluders.Occluder;

public interface LightSystem {
	public default void update(List<Light> lights, List<Occluder> occluders) {
		for(Light light : lights) {
			update(light, occluders);
		}
	}
	public void update(Light light, List<Occluder> occluders);
	
	public default void render(List<Light> lights, Batch batch) {
		for(Light light : lights) {
			render(light, batch);
		}
	}
	public void render(Light light, Batch batch);
}
