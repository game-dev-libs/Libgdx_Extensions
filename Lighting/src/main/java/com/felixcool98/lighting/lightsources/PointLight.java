package com.felixcool98.lighting.lightsources;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class PointLight implements Light {
	private int x, y;
	private Color color;
	private int size;
	private float dropOff;
	
	
	public PointLight(int x, int y, Color color, int size) {
		setX(x);
		setY(y);
		setColor(color);
		setSize(size);
	}
	
	
	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	
	/**
	 * can be used to add dropOff trough occluders
	 * 
	 * @param dropOff
	 */
	public void setDropOff(float dropOff) {
		this.dropOff = dropOff;
	}
	
	
	public void debugRender(ShapeRenderer shape) {
		shape.circle(x, y, size/2f);
	}
	
	
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	
	public Color getColor() {
		return color;
	}
	
	public int getSize() {
		return size;
	}
	
	public float getDropOff() {
		return dropOff;
	}
}
