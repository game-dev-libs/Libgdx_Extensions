package com.felixcool98.lighting.occluders;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public interface Occluder {
	/**
	 * draws the occluder<br>
	 * is only used for lighting and will never actually be displayed<br>
	 * the occluder must draw itself at its real world coordinates<br>
	 * transparent pixels will be ingored and wont occlude<br>
	 * larger areas are preferred
	 * 
	 * @param batch the batch used to draw the occluder {@link Batch#begin()} is already called
	 * @param shape the shaperrenderer used to draw the occluder {@link Batch#end()} and {@link ShapeRenderer#begin()} must be called before drawing
	 */
	public void drawOcculuder(Batch batch, ShapeRenderer shape);
}
