package com.felixcool98.lighting.occluders;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.felixcool98.aabb.shapes.Rectangle;

public class RectangleOccluder implements Occluder {
	private Rectangle rectangle;
	
	
	public RectangleOccluder(float x, float y, float width, float height) {
		this(new Rectangle(x, y, width, height));
	}
	public RectangleOccluder(Rectangle rectangle) {
		this.rectangle = rectangle;
	}
	
	
	@Override
	public void drawOcculuder(Batch batch, ShapeRenderer shape) {
		batch.end();
		shape.begin(ShapeType.Filled);
		shape.rect(rectangle.getX(), rectangle.getY(), rectangle.getWidth(), rectangle.getHeight());
		shape.end();
		batch.begin();
	}
}
