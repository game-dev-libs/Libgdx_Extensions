package com.felixcool98.lighting.pixelperfect;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.lighting.lightsources.PointLight;
import com.felixcool98.lighting.occluders.Occluder;

public class PointLightHandler implements LightHandler<PointLight> {
	private static ShaderProgram shadowRenderShader;
	private static ShaderProgram shadowMapShader;
	
	static {
		shadowMapShader = GdxUtils.createShader(Gdx.files.classpath("com/felixcool98/lighting/shadowmap.vertex"), Gdx.files.classpath("com/felixcool98/lighting/shadowmap.fragment"));
		shadowRenderShader = GdxUtils.createShader(Gdx.files.classpath("com/felixcool98/lighting/shadowrender.vertex"), Gdx.files.classpath("com/felixcool98/lighting/shadowrender.fragment"));
	}
	
	
	private OrthographicCamera camera;
	private Batch batch;
	private ShapeRenderer shape;
	
	
	private Map<PointLight, PointLightInstance> instances = new HashMap<>();
	
	
	public PointLightHandler() {
		camera = new OrthographicCamera();
		
		batch = new SpriteBatch();
		shape = new ShapeRenderer();
		shape.setAutoShapeType(true);
	}
	
	
	@Override
	public void render(PointLight light, Batch batch) {
		getInstance(light).render(batch, shadowRenderShader);
	}
	
	@Override
	public void update(PointLight light, List<Occluder> occluders) {
		getInstance(light).update(occluders, camera, batch, shape, shadowMapShader);
	}
	
	@Override
	public void debugDraw(PointLight light, Batch batch, ShapeRenderer shape) {
		getInstance(light).debugDraw(batch, shape);
	}
	
	
	private PointLightInstance getInstance(PointLight light) {
		if(!instances.containsKey(light))
			instances.put(light, new PointLightInstance(light));
		
		return instances.get(light);
	}
	
	
	@Override
	public Class<PointLight> getLightClass() {
		return PointLight.class;
	}
}
