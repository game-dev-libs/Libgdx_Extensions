package com.felixcool98.lighting.pixelperfect;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.lighting.lightsources.PointLight;
import com.felixcool98.lighting.occluders.Occluder;

public class PointLightInstance {
	private PointLight light;
	
	private TextureRegion shadowMap1D;
	private FrameBuffer occludersFBO;
	private TextureRegion occludersTexture;
	
	
	public PointLightInstance(PointLight light) {
		this.light = light;
	}
	
	
	public void render(Batch batch, ShaderProgram shadowRenderShader) {
		//set the shader which actually draws the light/shadow 
		batch.setShader(shadowRenderShader);

		//give shader our light size (256 by default)
		shadowRenderShader.setUniformf("resolution", light.getSize(), light.getSize());
		shadowRenderShader.setUniformf("max_dist", light.getDropOff());

		//set color to light
		batch.setColor(light.getColor());

		//draw light centered on light position
		batch.draw(shadowMap1D.getTexture(), light.getX()-light.getSize()/2f, light.getY()-light.getSize()/2f, light.getSize(), light.getSize());

		//reset color
		batch.setShader(null);
		batch.setColor(Color.WHITE);
	}
	public void debugDraw(Batch batch, ShapeRenderer shape) {
		float
			x = 1,
			y = 1;
		
		shape.begin();
		shape.setColor(Color.BLUE);
		shape.set(ShapeType.Filled);
		shape.rect(x, y, shadowMap1D.getRegionWidth(), 16);
		shape.end();
		
		batch.begin();
		batch.draw(shadowMap1D, x, y);
		batch.draw(shadowMap1D, x, y, shadowMap1D.getRegionWidth(), 16);
		batch.draw(occludersTexture, x, y+16);
		batch.end();
		
		Gdx.gl.glLineWidth(1);
		shape.begin();
		shape.setColor(Color.RED);
		shape.set(ShapeType.Line);
		shape.rect(x, y, occludersTexture.getRegionWidth()+1, occludersTexture.getRegionHeight()+16);
		shape.setColor(Color.WHITE);
		shape.end();
	}	
	
	
	public void update(List<Occluder> occluders, OrthographicCamera camera, Batch batch, ShapeRenderer shape, ShaderProgram shadowMapShader) {
		createOcclusionMap(occluders, camera, batch, shape);
		createShadowMap(shadowMapShader, camera, batch);
	}
	
	private void createOcclusionMap(List<Occluder> occluders, OrthographicCamera camera, Batch batch, ShapeRenderer shape) {
		//create a FrameBufferObject with proper format and no depth
		occludersFBO = new FrameBuffer(Format.RGBA8888, light.getSize(), light.getSize(), false);
		
		//get color buffer texture of FBO for region
		occludersTexture = new TextureRegion(occludersFBO.getColorBufferTexture());
		//flip it on Y-axis due to OpenGL coordinate system
		occludersTexture.flip(false, true);
		
		//bind the occluder FBO
		occludersFBO.begin();

		//clear the FBO
		GdxUtils.clearScreen(0, 0, 0, 0);

		//set the orthographic camera to the size of our FBO
		camera.setToOrtho(false, occludersFBO.getWidth(), occludersFBO.getHeight());

		//translate camera so that light is in the center 
		camera.translate(light.getX() - light.getSize()/2f, light.getY() - light.getSize()/2f);

		//update camera matrices
		camera.update();

		//set up our batch for the occluder pass
		batch.setProjectionMatrix(camera.combined);
		shape.setProjectionMatrix(camera.combined);

		//reset to default shader
		batch.setShader(null);
		batch.begin();

		for(Occluder occluder : occluders) {
			occluder.drawOcculuder(batch, shape);
		}

		//end the batch before unbinding the FBO
		batch.end();

		//unbind the FBO
		occludersFBO.end();
	}
	private void createShadowMap(ShaderProgram shadowMapShader, OrthographicCamera camera, Batch batch) {
		//our 1D shadow map, lightSize x 1 pixels, no depth
		FrameBuffer shadowMapFBO = new FrameBuffer(Format.RGBA8888, light.getSize(), 1, false);
		Texture shadowMapTex = shadowMapFBO.getColorBufferTexture();

		//use linear filtering and repeat wrap mode when sampling
		shadowMapTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		shadowMapTex.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		
		//for debugging only; in order to render the 1D shadow map FBO to screen
		shadowMap1D = new TextureRegion(shadowMapTex);
		shadowMap1D.flip(false, true);
		
		//bind shadow map FBO
		shadowMapFBO.begin();

		//clear it
		GdxUtils.clearScreen(0, 0, 0, 0);

		//set our shadow map shader
		batch.setShader(shadowMapShader);

		//start rendering
		batch.begin();

		//give the shader our lightSize resolution
		shadowMapShader.setUniformf("resolution", light.getSize(), light.getSize());
		shadowMapShader.setUniformf("upScale", 1);

		//reset our projection matrix to the FBO size
		camera.setToOrtho(false, shadowMapFBO.getWidth(), shadowMapFBO.getHeight());
		batch.setProjectionMatrix(camera.combined);

		//draw the occluders texture to our 1D shadow map FBO
		batch.draw(occludersFBO.getColorBufferTexture(), 0, 0, light.getSize(), shadowMapFBO.getHeight());

		//flush batch
		batch.end();

		//unbind shadow map FBO
		shadowMapFBO.end();
	}
}
