package com.felixcool98.logging.logtype;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.utility.filter.Filter;

public class SpecificLevelFilter implements Filter<Integer> {
	private List<Integer> values = new LinkedList<>();
	
	
	public SpecificLevelFilter(int[] loglevels) {
		for(int level : loglevels) {
			values.add(level);
		}
	}
	public SpecificLevelFilter(LogType[] logtypes) {
		for(LogType type : logtypes) {
			values.add(type.level);
		}
	}
	
	
	@Override
	public boolean valid(Integer object) {
		if(!values.contains(object))
			return false;
		
		return true;
	}

}
