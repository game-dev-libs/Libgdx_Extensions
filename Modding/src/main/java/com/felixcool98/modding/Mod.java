package com.felixcool98.modding;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Mod {
	private Map<Class<?>, List<?>> objects = new HashMap<>();
	
	private List<ModObjectRegisteryListener> registeredListeners = new LinkedList<>();
	
	
	@SuppressWarnings("unchecked")
	public <A> void register(A object) {
		if(!objects.containsKey(object.getClass())) {
			objects.put(object.getClass(), new LinkedList<A>());
		}
		
		((List<A>) objects.get(object.getClass())).add(object);
		
		registered(object);
	}
	private void registered(Object obj) {
		List<ModObjectRegisteryListener> remove = new LinkedList<>();
		
		for(ModObjectRegisteryListener listener : registeredListeners) {
			if(listener.registered(obj))
				remove.add(listener);
		}
		
		registeredListeners.removeAll(remove);
	}
	
	
	@SuppressWarnings("unchecked")
	public <A> List<A> get(Class<A> classs) {
		List<A> list = objects.get(classs) != null ? new LinkedList<>((List<A>) objects.get(classs)) : new LinkedList<>();
		
		return list;
	}
	
	
	public static interface ModObjectRegisteryListener {
		/**
		 * 
		 * 
		 * @param obj
		 * @return true if the listener should be removed
		 */
		public boolean registered(Object obj);
	}
}
