package com.felixcool98.modding;

public class ModException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7146053199065187970L;
	
	public ModException(String arg0) {
		super(arg0);
	}
}
