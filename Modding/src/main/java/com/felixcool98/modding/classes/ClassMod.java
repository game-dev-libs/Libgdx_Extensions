package com.felixcool98.modding.classes;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.felixcool98.modding.Mod;
import com.felixcool98.modding.ModException;

public class ClassMod<T extends Annotation> extends Mod {
	private Class<?> classs;
	
	private T annotation;
	
	private Field[] leftFields;
	
	private Map<Integer, List<Method>> methods = new HashMap<>();
	private int highestOrder = 0;
	
	
	public ClassMod(Class<?> classs, Class<T> annotation, Class<?>... classes) throws ModException{
		this.classs = classs;
		this.annotation = classs.getAnnotation(annotation);
		
		List<Field> fields = new LinkedList<>();
		
		for(Field field : classs.getFields()) {
			for(Class<?> checkClass : classes) {
				if(!checkClass.isAssignableFrom(field.getType()))
					continue;
				
				fields.add(field);
				
				break;
			}
		}
		
		this.leftFields = fields.toArray(new Field[0]);
		sortMethods(classs.getMethods());
	}
	
	private void sortMethods(Method[] methods) throws ModException {
		List<Method> sorted = new LinkedList<>();
		
		for(Method method : methods) {
			if(method == null)
				continue;
			
			if(!method.isAnnotationPresent(Init.class))
				continue;
			
			sorted.add(method);
			
			Init init = method.getAnnotation(Init.class);
			
			if(init.order() < 0 || init.localOrder() < 0) 
				throw new ModException("Method " + method.getName() + " in " +classs.getCanonicalName()+ ": order can't be negative");
		}
		
		sorted.sort(new Comparator<Method>() {
			@Override
			public int compare(Method method, Method other) { 
				if(!method.isAnnotationPresent(Init.class) || !other.isAnnotationPresent(Init.class))
					return -1;
				
				if(method.getAnnotation(Init.class).localOrder() < other.getAnnotation(Init.class).localOrder())
					return -1;
				
				if(method.getAnnotation(Init.class).localOrder() > other.getAnnotation(Init.class).localOrder())
					return 1;
			
				return 0;
			}
		});
		
		for(Method method : sorted) {
			put(method, method.getAnnotation(Init.class).order());
			
			if(method.getAnnotation(Init.class).order() > highestOrder) {
				highestOrder = method.getAnnotation(Init.class).order();
				
			}
		}
	}
	private void put(Method method, int order) {
		if((methods.get(order) == null)) {
			methods.put(order, new LinkedList<>());
		}
		
		methods.get(order).add(method);
	}
	
	
	
	
	
	public int fieldsLeft() {
		return leftFields.length;
	}
	
	public int highestOrder() {
		return highestOrder;
	}
	
	public T getAnnotation() {
		return annotation;
	}
	
	public Class<?> getOriginalClass(){
		return classs;
	}
	
	
	/**
	 * checks if 
	 * 
	 * @return when every field is done returns true
	 */
	public boolean processFields() {
		if(leftFields.length == 0)
			return true;
		
		List<Field> fields = new LinkedList<>();
		
		for(Field field : leftFields) {
			try {
				if(field.get(null) == null) {
					fields.add(field);
					continue;
				}
				
				this.register(field.get(null));
			} catch (IllegalArgumentException e1) {
				e1.printStackTrace();
			} catch (IllegalAccessException e1) {
				e1.printStackTrace();
			}
		}
		
		this.leftFields = fields.toArray(new Field[0]);
		
		return fields.isEmpty();
	}
	public void executeMethods(int order) {
		if(methods.get(order) == null)
			return;
		
		for(Method method : methods.get(order)) {
			try {
				method.invoke(null);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		
		methods.remove(order);
	}
	
	
	@Override
	public String toString() {
		return classs.getCanonicalName();
	}
}
