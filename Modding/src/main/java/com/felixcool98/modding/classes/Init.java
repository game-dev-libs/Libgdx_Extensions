package com.felixcool98.modding.classes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)

public @interface Init {
	int order() default 0;
	int localOrder() default 0;
}
