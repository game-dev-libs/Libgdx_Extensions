package com.felixcool98.modding.directory;

import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.modding.Mod;
import com.felixcool98.resources.Directory;
import com.felixcool98.resources.Resources;

public class ModDirectory extends Mod {
	private Directory directory;
	
	
	public ModDirectory(FileHandle handle) {
		directory = Resources.getDirectory(handle);
	}
	
	
	public Directory getDirectory() {
		return directory;
	}
}
