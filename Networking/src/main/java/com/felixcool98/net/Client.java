package com.felixcool98.net;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.felixcool98.utility.threads.ThreadUtils;

public abstract class Client implements Net {
	private com.esotericsoftware.kryonet.Client client;
	
	private boolean running = false;
	
	private List<ConnectionListener> connectionListeners = new LinkedList<>();
	
	
	public Client(int bufferSize) {
		this(bufferSize, bufferSize);
	}
	/**
	 * 
	 * @param writeBuffer default 8192 
	 * @param objectBuffer default 2048
	 */
	public Client(int writeBuffer, int objectBuffer) {
		client = new com.esotericsoftware.kryonet.Client(writeBuffer, objectBuffer);
		init();
	}
	public Client() {
		client = new com.esotericsoftware.kryonet.Client();
		init();
	}
	
	private void init() {
		registerClasses(client.getKryo());
		client.addListener(new Listener() {
			@Override
			public void disconnected(Connection connection) {
				ThreadUtils.runSynchronous(()->{
					for(ConnectionListener listener : connectionListeners) {
						listener.disconnected();
					}
				});
				
			}
			@Override
			public void received(Connection connection, Object object) {
				ThreadUtils.runSynchronous(()->{
					Client.this.received(connection, object);
				});
			}
			@Override
			public void connected(Connection connection) {
				ThreadUtils.runSynchronous(()->{
					for(ConnectionListener listener : connectionListeners) {
						listener.connected();
					}
				});
			}
		});
	}
	
	
	public void connect(String host, int port) {
		if(isRunning())
			disconnect();
		
		client.start();
		
		try {
			client.connect(5000, host, port);
			
			running = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void disconnect() {
		if(!isRunning())
			return;

		client.stop();
		
		running = false;
	}
	
	
	public abstract void received(Connection connection, Object object);
	
	
	public com.esotericsoftware.kryonet.Client getClient() {
		return client;
	}
	public boolean isRunning() {
		return running;
	}
	
	
	//======================================================================
	// listeners
	//======================================================================
	public void addListener(ConnectionListener listener) {
		connectionListeners.add(listener);
	}
	
	
	
	public static interface ConnectionListener {
		public void connected();
		public void disconnected();
	}
}
