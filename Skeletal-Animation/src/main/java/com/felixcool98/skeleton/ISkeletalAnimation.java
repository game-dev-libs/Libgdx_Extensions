package com.felixcool98.skeleton;

public interface ISkeletalAnimation {
	public void setFPS(int fps);
	public int getFPS();
	
	public void addAngleToBone(String boneName, float angle, float time, float startTime);
	
	
	public static abstract class SkeletalAnimationImpl implements ISkeletalAnimation {
		private int fps = 60;
		
		
		//======================================================================
		// setter
		//======================================================================
		@Override
		public void setFPS(int fps) {
			this.fps = fps;
		}

		
		//======================================================================
		// getter
		//======================================================================
		@Override
		public int getFPS() {
			return fps;
		}
	}
}
