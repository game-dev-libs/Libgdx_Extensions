package com.felixcool98.skeleton;

import com.felixcool98.utility.builder.Builder;

public class SkeletonBuilder extends Builder<SkeletonInstance> {
	private SkeletonInstance skeleton = new SkeletonInstance();
	
	
	private Bone parent;
	private Bone current;
	
	private boolean autoChangeParent = true;
	
	
	//======================================================================
	// adding and setting bone
	//======================================================================
	public SkeletonBuilder addBone(String name) {
		if(current == null)
			current = skeleton.setFirstBone(name);
		else {
			if(autoChangeParent)
				parent = current;
			
			current = parent.addChild(name);
		}
		
		return this;
	}
	public SkeletonBuilder setParent(String name) {
		parent = skeleton.getBone(name);
		
		return this;
	}
	public SkeletonBuilder setCurrent(String name) {
		current = skeleton.getBone(name);
		
		return this;
	}
	
	
	//======================================================================
	// setter
	//======================================================================
	public SkeletonBuilder setData(float angleOnParent, float length, float position) {
		current.setData(angleOnParent, length, position);
		
		return this;
	}
	
	
	//======================================================================
	// drawing
	//======================================================================
	public SkeletonBuilder setDepth(int depth) {
		current.setDepth(depth);
		
		return this;
	}
	
	
	@Override
	public SkeletonInstance build() {
		return skeleton;
	}

}
