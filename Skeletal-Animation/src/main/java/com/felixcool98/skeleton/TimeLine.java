package com.felixcool98.skeleton;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.felixcool98.gdxutility.GdxUtils;

public class TimeLine extends Widget {
	private int fps = 60;
	
	private ShapeRenderer renderer;
	
	private int frameOffSet = 0;
	
	private float currentFrame = 0;
	private List<FrameChangeListener> frameChangeListeners = new LinkedList<>();
	
	private boolean running = false;
	private float runSpeed = 1;
	private float pixelPerFps = 1;
	
	
	public TimeLine() {
		super();
		
		renderer = new ShapeRenderer();
		renderer.setAutoShapeType(true);
	}
	
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
		if(running)
			addToCurrentFrame(GdxUtils.getDeltaTime()*runSpeed);
		
		batch.end();
		batch.flush();
		
		renderer.setProjectionMatrix(batch.getProjectionMatrix());
		
		renderer.begin();{
			drawLines(renderer);
		}renderer.end();
		
		batch.begin();
	}
	private void drawLines(ShapeRenderer renderer) {
		float start = getX();
		
		for(float i = 0; i < getWidth()/pixelPerFps; i++) {
			int frame = (int) ((frameOffSet + i));
			
			float x = start + i * pixelPerFps;
			
			if(frame == getCurrentFrame()) {
				renderer.setColor(Color.RED);
				renderer.line(x, getY(), x, getY()+getHeight());
			}else if(frame%fps == 0) {
				renderer.setColor(Color.WHITE);
				renderer.line(x, getY(), x, getY()+getHeight());
			}else if(frame%(fps/4) == 0) {
				renderer.setColor(Color.WHITE);
				renderer.line(x, getY(), x, getY()+getHeight()/2f);
			}
		}
	}
	
	
	//======================================================================
	// setter
	//======================================================================
	public void setFrameOffSet(int offSet) {
		frameOffSet = offSet;
	}
	public void setTimeOffSet(float time) {
		frameOffSet = (int) (time*fps);
	}
	
	public void setFps(int fps) {
		this.fps = fps;
	}
	
	public void setCurrentFrame(float currentFrame) {
		if(currentFrame < 0)
			return;
		
		this.currentFrame = currentFrame;
		
		frameChanged();
	}
	public void addToCurrentFrame(float amount) {
		setCurrentFrame(currentFrame + amount*fps);
	}
	public void addToCurrentFrame(int amount) {
		setCurrentFrame(currentFrame + (float)amount);
	}
	
	public void setRunning(boolean state) {
		running = state;
	}
	public void play() {
		setRunning(true);
	}
	public void pause() {
		setRunning(false);
	}
	
	public void setPixelPerFps(float pixelPerFps) {
		this.pixelPerFps = pixelPerFps;
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	public int getFps() {
		return fps;
	}
	
	
	public int getFrameAt(float x) {
		return (int) Math.round((x + frameOffSet)/pixelPerFps);
	}
	public float getTimeAt(float x) {
		return getFrameAt(x)/getFps();
	}
	
	public int getCurrentFrame() {
		return (int) (currentFrame);
	}
	public float getCurrentTime() {
		return (float)getCurrentFrame() / (float)fps;
	}
	
	
	//======================================================================
	// listeners
	//======================================================================
	public void addListener(FrameChangeListener listener) {
		frameChangeListeners.add(listener);
	}
	public void removeListener(FrameChangeListener listener) {
		frameChangeListeners.remove(listener);
	}
	private void frameChanged() {
		for(FrameChangeListener listener : frameChangeListeners) {
			listener.frameChanged(getCurrentFrame(), getCurrentTime());
		}
	}
	
	
	public static interface FrameChangeListener {
		public void frameChanged(int frame, float time);
	}
}
