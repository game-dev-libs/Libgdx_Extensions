package com.felixcool98.skeleton.image;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Polygon;
import com.felixcool98.math.VectorMath;
import com.felixcool98.skeleton.Bone;

public class ImageBone extends Bone {
	private TextureRegion image;
	
	private float imageWidth;
	private float imageHeight;
	private float imageAngle;
	private float imageOffSetX;
	private float imageOffSetY;
	
	private int depth;
	
	
	public ImageBone(String name) {
		super(name);
	}
	
	
	public void setAll(ImageBone bone) {
		super.setAll(bone);
		
		image = bone.image;
		
		imageWidth = bone.imageWidth;
		imageHeight = bone.imageHeight;
		imageAngle = bone.imageAngle;
		imageOffSetX = bone.imageOffSetX;
		imageOffSetY = bone.imageOffSetY;
		
		depth = bone.depth;
	}
	
	
	//======================================================================
	// checks
	//======================================================================
	public ImageBone hitAnything(float x, float y, float offSetX, float offSetY) {
		if(hitMe(x, y, offSetX, offSetY))
			return this;
		
		for(ImageBone child : getImageChildren()) {
			ImageBone hit = child.hitAnything(x, y, offSetX, offSetY);
			
			if(hit == null)
				continue;
			
			return hit;
		}
		
		return null;
	}
	
	public boolean hitMe(float x, float y, float offSetX, float offSetY) {
		Polygon polygon = getPolygon();
		polygon.setPosition(polygon.getX()+offSetX, polygon.getY()+offSetY);
		
		return polygon.contains(x, y);
	}
	public boolean hitMe(float x, float y) {
		return hitMe(x, y, 0, 0);
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	
	//children
	public List<ImageBone> getImageChildren() {
		List<Bone> children = getChildren();
		
		List<ImageBone> imageChildren = new ArrayList<>(children.size());
		
		for(Bone child : children) {
			imageChildren.add((ImageBone) child);
		}
		
		return imageChildren;
	}
	public List<ImageBone> getAllImageChildren() {
		List<Bone> children = getAllBones();
		
		List<ImageBone> imageChildren = new ArrayList<>(children.size());
		
		for(Bone child : children) {
			imageChildren.add((ImageBone) child);
		}
		
		return imageChildren;
	}
	
	//drawing
	public float getImageWidth() {
		return imageWidth;
	}
	public float getImageHeight() {
		return imageHeight;
	}
	
	public float getImageOffSetX() {
		return imageOffSetX;
	}
	public float getImageOffSetY() {
		return imageOffSetY;
	}
	
	public float getImageAngle() {
		return imageAngle;
	}
	
	public Polygon getPolygon() {
		return getPolygon(getParentX(), getParentY(), getParentAngle());
	}
	public Polygon getPolygon(float parentX, float parentY, float parentAngle) {
		Polygon polygon = new Polygon();
		polygon.setVertices(new float[] {
				0, 0,
				getImageWidth(), 0,
				getImageWidth(), getImageHeight(),
				0, getImageHeight()
		});
		
		float localX = getLocalX(getImageOffSetX(), getImageOffSetY(), parentAngle, getImageAngle());
		float localY = getLocalY(getImageOffSetX(), getImageOffSetY(), parentAngle, getImageAngle());
		
		float x = getX(parentX, parentAngle) + localX;
		float y = getY(parentY, parentAngle) + localY;
		
		float rotation = getAngle(parentAngle) + getImageAngle();
		
		polygon.setPosition(x, y);
		polygon.setRotation(rotation);
		
		return polygon;
	}
	
	private float getLocalX(float x, float y, float angleOff) {
		if(getParent() == null)
			return getLocalX(x, y, 0, angleOff);
		
		return getLocalX(x, y, getParent().getAngle(), angleOff);
	}
	private float getLocalX(float x, float y, float parentAngle, float angleOff) {
		float pointAngle = VectorMath.vectorAngle(x, y);
		float pointLength = VectorMath.vectorLength(x, y);
		
		return VectorMath.vectorX(getAngle(parentAngle+angleOff)+pointAngle, pointLength);
	}
	private float getLocalY(float x, float y, float angleOff) {
		if(getParent() == null)
			return getLocalY(x, y, 0, angleOff);
		
		return getLocalY(x, y, getParent().getAngle(), angleOff);
	}
	private float getLocalY(float x, float y, float parentAngle, float angleOff) {
		float pointAngle = VectorMath.vectorAngle(x, y);
		float pointLength = VectorMath.vectorLength(x, y);
		
		return VectorMath.vectorY(getAngle(parentAngle+angleOff)+pointAngle, pointLength);
	}
	
	public int getDepth() {
		return depth;
	}
	
	
	//======================================================================
	// setter
	//======================================================================
	public void setImage(TextureRegion image, float x, float y, float width, float height, float angle) {
		setImage(image);
		setImageData(x, y, width, height, angle);
	}
	
	public void setImage(TextureRegion image) {
		this.image = image;
	}
	
	public void setImageData(float x, float y, float width, float height, float angle) {
		setImageOffSet(x, y);
		setImageSize(width, height);
		setImageAngle(angle);
	}
	
	public void setImageWidth(float width) {
		imageWidth = width;
	}
	public void setImageHeight(float height) {
		imageHeight = height;
	}
	public void setImageSize(float width, float height) {
		setImageWidth(width);
		setImageHeight(height);
	}
	
	public void setImageAngle(float angle) {
		imageAngle = angle;
	}
	
	public void setImageOffSetX(float x) {
		imageOffSetX = x;
	}
	public void setImageOffSetY(float y) {
		imageOffSetY = y;
	}
	public void setImageOffSet(float x, float y) {
		setImageOffSetX(x);
		setImageOffSetY(y);
	}
	
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	
	//======================================================================
	// drawing
	//======================================================================
	public void render(Batch batch, float offSetX, float offSetY) {
		if(image == null)
			return;
		
		float localX = getLocalX(imageOffSetX, imageOffSetY, imageAngle);
		float localY = getLocalY(imageOffSetX, imageOffSetY, imageAngle);
		float x = getX() + localX+offSetX;
		float y = getY() + localY+offSetY;
		
		float scaleX = imageWidth / image.getRegionWidth();
		float scaleY = imageHeight / image.getRegionHeight();
		
		float rotation = getAngle() + imageAngle;
		
		batch.begin();{
			batch.draw(image, x, y, 0, 0, image.getRegionWidth(), image.getRegionHeight(), scaleX, scaleY, rotation);
		}batch.end();
	}
	public void render(Batch batch, float parentX, float parentY, float parentAngle) {
		for(ImageBone child : getImageChildren()) {
			child.render(batch, getX(parentX, parentAngle), getY(parentY, parentAngle), getAngle(parentAngle));
		}
		
		if(image == null)
			return;
		
		float localX = getLocalX(imageOffSetX, imageOffSetY, parentAngle, imageAngle);
		float localY = getLocalY(imageOffSetX, imageOffSetY, parentAngle, imageAngle);
		float x = getX(parentX, parentAngle) + localX;
		float y = getY(parentY, parentAngle) + localY;
		
		float scaleX = imageWidth / image.getRegionWidth();
		float scaleY = imageHeight / image.getRegionHeight();
		
		float rotation = getAngle(parentAngle) + imageAngle;
		
		batch.begin();{
			batch.draw(image, x, y, 0, 0, image.getRegionWidth(), image.getRegionHeight(), scaleX, scaleY, rotation);
		}batch.end();
	}
	public void renderDebugImageOutlines(ShapeRenderer renderer, float parentX, float parentY, float parentAngle) {
		renderer.setAutoShapeType(true);
		
		for(ImageBone child : getImageChildren()) {
			child.renderDebugImageOutlines(renderer, getX(parentX, parentAngle), getY(parentY, parentAngle), getAngle(parentAngle));
		}
		
		Polygon polygon = getPolygon(parentX, parentY, parentAngle);
		
		renderer.begin();{
			renderer.polygon(polygon.getTransformedVertices());
		}renderer.end();
	}
	
	
	//======================================================================
	// children
	//======================================================================
	@Override
	@Deprecated
	public Bone addChild(Bone child) {
		// TODO Auto-generated method stub
		return super.addChild(child);
	}
	
	
	public ImageBone addChild(ImageBone child) {
		super.addChild(child);
		
		return child;
	}
	@Override
	public ImageBone addChild(String name) {
		return addChild(new ImageBone(name));
	}
	
	public ImageBone getChild(String name) {
		return (ImageBone) super.getChild(name);
	}
}