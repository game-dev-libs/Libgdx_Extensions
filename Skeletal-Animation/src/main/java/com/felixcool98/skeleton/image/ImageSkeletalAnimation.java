package com.felixcool98.skeleton.image;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.skeleton.ISkeletalAnimation.SkeletalAnimationImpl;

public class ImageSkeletalAnimation extends SkeletalAnimationImpl {
	private List<ImageSkeletonInstance> images = new ArrayList<>();
	
	private float length = 0;
	
	private ImageSkeletonInstance start;
	
	
	public ImageSkeletalAnimation(ImageSkeletonInstance start) {
		initalize(start);
	}
	
	protected void initalize(ImageSkeletonInstance start) {
		this.start = start;
		images.add(start);
	}
	
	
	public void setStart(ImageSkeletonInstance start) {
		this.start = start;
	}
	

	public void render(Batch batch, float x, float y, float angle, float time) {
		ImageSkeletonInstance skeleton = getInstanceAtTime(time);
		
		if(skeleton == null)
			return;
		
		skeleton.render(batch, x, y, angle);
	}
	public void renderDebugImageOutlines(ShapeRenderer renderer, float x, float y, float angle, float time) {
		ImageSkeletonInstance skeleton = getInstanceAtTime(time);
		
		if(skeleton == null)
			return;
		
		skeleton.renderDebugImageOutlines(renderer, x, y, angle);
	}
	
	
	@Override
	public void addAngleToBone(String boneName, float angle, float time, float startTime) {
		if(start.getBone(boneName) == null)
			return;
		
		if(startTime + time > length)
			length = startTime + time;
		
		int startImage = (int) (startTime*getFPS());
		int images = (int) (time*getFPS());
		float anglePerImage = angle/(float) images;
		
		for(int i = 0; i < startImage+images; i++) {
			if(i < this.images.size())
				continue;
			
			this.images.add(this.images.get(i-1).clone());
			this.images.set(i, this.images.get(i-1).clone());
		}
		
		float currentAngle = startImage > 0 
				? this.images.get(startImage-1).getBone(boneName).getAngleOnParent() : 
					this.images.get(startImage).getBone(boneName).getAngleOnParent();
		for(int i = 0; i < images; i++) {
			this.images.get(startImage+i).getBone(boneName).setAngleOnParent(currentAngle);
			currentAngle += anglePerImage;
		}
	}
	
	
	public ImageSkeletonInstance getInstanceAtTime(float time) {
		time = time*getFPS();
		int image = (int) (time%(getFPS()*length));
		return images.get(image);
	}
}
