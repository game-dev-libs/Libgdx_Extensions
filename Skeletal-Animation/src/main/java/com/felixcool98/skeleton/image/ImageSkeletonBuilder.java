package com.felixcool98.skeleton.image;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.felixcool98.gdxutility.textures.TextureManager;
import com.felixcool98.utility.builder.Builder;

public class ImageSkeletonBuilder extends Builder<ImageSkeletonInstance> {
	private ImageSkeletonInstance skeleton = new ImageSkeletonInstance();
	
	
	private ImageBone parent;
	private ImageBone current;
	
	private boolean autoChangeParent = true;
	private boolean skipAutoChange = false;
	
	
	//======================================================================
	// adding and setting bone
	//======================================================================
	public ImageSkeletonBuilder addBone(String name) {
		if(current == null)
			current = skeleton.setFirstBone(name);
		else {
			if(autoChangeParent && !skipAutoChange)
				parent = current;
			
			skipAutoChange = false;
			
			current = parent.addChild(name);
		}
		
		return this;
	}
	public ImageSkeletonBuilder setParent(String name) {
		parent = skeleton.getBone(name);
		
		skipAutoChange = true;
		
		return this;
	}
	public ImageSkeletonBuilder setCurrent(String name) {
		current = skeleton.getBone(name);
		
		return this;
	}
	
	
	//======================================================================
	// setter
	//======================================================================
	public ImageSkeletonBuilder setData(float angleOnParent, float length, float position) {
		current.setData(angleOnParent, length, position);
		
		return this;
	}
	
	
	//======================================================================
	// drawing
	//======================================================================
	public ImageSkeletonBuilder setImage(TextureRegion image) {
		current.setImage(image);
		
		return this;
	}
	public ImageSkeletonBuilder setImage(String path) {
		return setImage(TextureManager.INSTANCE.getTextureRegion(path));
	}
	
	public ImageSkeletonBuilder setImageData(float x, float y, float width, float height, float angle) {
		current.setImageData(x, y, width, height, angle);
		
		return this;
	}
	
	public ImageSkeletonBuilder setDepth(int depth) {
		current.setDepth(depth);
		
		return this;
	}
	
	
	@Override
	public ImageSkeletonInstance build() {
		return skeleton;
	}
}	
