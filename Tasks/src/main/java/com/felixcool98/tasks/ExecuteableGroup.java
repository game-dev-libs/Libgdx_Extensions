package com.felixcool98.tasks;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.logging.Logs;
import com.felixcool98.logging.logtype.LogType;

public class ExecuteableGroup implements Executeable {
	public static final LogType TASK = new LogType(5, "Task");
	
	
	private List<Executeable> executeables = new LinkedList<>();
	
	private float currentProgress = 0;
	private Executeable currentExecuteable;
	
	private List<LTaskExecuted> listeners = new LinkedList<>();
	
	
	public void add(Executeable executeable) {
		if(executeable == null)
			return;
		
		executeables.add(executeable);
	}
	
	
	@Override
	public String getDisplayText() {
		if(currentExecuteable == null)
			return executeables.get(0).getDisplayText();
		
		return currentExecuteable.getDisplayText();
	}

	@Override
	public void execute() {
		for(Executeable executeable : executeables) {
			currentExecuteable = executeable;
			
			Logs.log(TASK, currentExecuteable.getDisplayText());
			
			executeable.execute();
			
			currentProgress += 1f/executeables.size();
			
			taskExecuted();
		}
	}

	@Override
	public float getProgress() {
		return currentProgress;
	}

	
	public int size() {
		return executeables.size();
	}
	
	public void taskExecuted() {
		for(LTaskExecuted listener : listeners) {
			listener.done(getProgress());
		}
	}
	@Override
	public void addListener(LTaskExecuted listener) {
		listeners.add(listener);
	}
}
