package com.felixcool98.tasks;

import java.util.HashMap;
import java.util.Map;

import com.felixcool98.logging.Logs;

public class TaskManager {
	public static final TaskManager instance = new TaskManager();
	
	
	private Map<String, ExecuteableGroup> tasks = new HashMap<>();
	
	
	public void add(String group, Executeable task) {
		if(!tasks.containsKey(group)) {
			tasks.put(group, new ExecuteableGroup());
		}

		tasks.get(group).add(task);
	}
	public void add(String group, LTaskExecuted listener) {
		if(!tasks.containsKey(group)) {
			tasks.put(group, new ExecuteableGroup());
		}
		
		tasks.get(group).addListener(listener);
	}
	
	
	public void execute(String group) {
		Logs.log("executing " + tasks.get(group).size() + " executeables");
		
		tasks.get(group).execute();
	}
	public void clear(String group) {
		tasks.remove(group);
	}
	
	
	public ExecuteableGroup get(String name) {
		if(!tasks.containsKey(name)) {
			tasks.put(name, new ExecuteableGroup());
		}
		
		return tasks.get(name);
	}
}
