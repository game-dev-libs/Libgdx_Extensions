package dialogdisplay;

import com.felixcool98.dialogs.Character;
import com.felixcool98.dialogs.Dialog;
import com.felixcool98.dialogs.SimpleDialog;
import com.felixcool98.dialogs.gui.CharacterDisplay.CharacterDisplayStyle;
import com.felixcool98.dialogs.gui.DialogDisplay;
import com.felixcool98.dialogs.gui.DialogDisplay.DialogDisplayStyle;
import com.felixcool98.dialogs.options.OptionOpenDialog;
import com.felixcool98.game.ImprovedGameState;

public class DialogDisplayTestImpl extends ImprovedGameState {
	@Override
	public void created() {
		DialogDisplayStyle style = new DialogDisplayStyle();{
			style.textHeight = 300;
			style.width = 600;
			
			style.characterDisplayStyle = new CharacterDisplayStyle();{
				style.characterDisplayStyle.width = 64;
				style.characterDisplayStyle.imageHeight = 64;
			}
		}
		
		DialogDisplay display = new DialogDisplay(style);
		Character player = new Character("player");
		Character npc = new Character("npc");
		
		Dialog dialog = new SimpleDialog(player, "default", npc, "default", "How is it going? How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?How is it going?");
		
		Dialog dialog2 = new SimpleDialog(player, "default", npc, "default", "2");
		dialog.addOption(new OptionOpenDialog("continue", dialog2));
		
		display.setDialog(dialog);
		display.setPosition(500, 500);
		
		getStage().addActor(display);
	}
}
