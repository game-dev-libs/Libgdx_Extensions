package dialogdisplay.twobranches;

import com.felixcool98.dialogs.Character;
import com.felixcool98.dialogs.Dialog;
import com.felixcool98.dialogs.SimpleDialog;
import com.felixcool98.dialogs.gui.DialogDisplay;
import com.felixcool98.dialogs.gui.CharacterDisplay.CharacterDisplayStyle;
import com.felixcool98.dialogs.gui.DialogDisplay.DialogDisplayStyle;
import com.felixcool98.dialogs.options.OptionOpenDialog;
import com.felixcool98.game.ImprovedGameState;

public class TwoBranches extends ImprovedGameState {
	@Override
	public void created() {
		DialogDisplayStyle style = new DialogDisplayStyle();{
			style.textHeight = 300;
			style.width = 600;
			style.buttonPaneHeight = 100;
			
			style.characterDisplayStyle = new CharacterDisplayStyle();{
				style.characterDisplayStyle.width = 64;
				style.characterDisplayStyle.imageHeight = 64;
			}
		}
		
		DialogDisplay display = new DialogDisplay(style);
		Character player = new Character("player");
		player.useDefaultImage();
		Character npc = new Character("npc");
		npc.useDefaultImage();
		
		Dialog dialog = new SimpleDialog(player, npc, "Are Dialogs working?");
		dialog.speakRight();
		Dialog optionYes = new SimpleDialog(player, npc, "They work really well.");
		Dialog optionNo = new SimpleDialog(player, npc, "But how can you see this message then?");
		dialog.addOption(new OptionOpenDialog("Yes", optionYes));
		dialog.addOption(new OptionOpenDialog("No", optionNo));
		
		display.setDialog(dialog);
		display.setPosition(500, 500);
		
		getStage().addActor(display);
		getStage().setDebugAll(true);
	}
}
