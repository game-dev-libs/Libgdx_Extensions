package gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.events.EventListener;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.GameWorldDrawable;
import com.felixcool98.gameworld.GameWorldObject;
import com.felixcool98.gameworld.MovingGameWorldObject;
import com.felixcool98.gameworld.draw.GameWorldView;
import com.felixcool98.gameworld.events.CollisionEvent;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.gdxutility.textures.TextureManager;
import com.felixcool98.launchutils.lwjgl3.LaunchUtilsLwjgl3;

public class CollisionTest extends ImprovedGameState {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		LaunchUtilsLwjgl3.launch(new CollisionTest(), config);
	}

	
	private GameWorld world;
	private GameWorldView view;
	
	private Batch batch;
	
	private ShapeRenderer renderer;
	
	private Entity player;
	
	
	@Override
	public void created() {
		setClearColor(Color.BLACK);
		
		TextureRegion region = TextureManager.INSTANCE.getTextureRegion(Gdx.files.classpath("com/felixcool98/tests/res/blue.png"));
		
		world = new GameWorld();
		
		player = new Entity(new Rectangle(0, 0, 1, 1), region);
		world.create(player);
		
		view = new GameWorldView(world, new OrthographicCamera(10, 10*GdxUtils.getAspectRatio()));
		
		view.getCamera().position.set(1, 1, 0);
		
		batch = new SpriteBatch();
		
		renderer = new ShapeRenderer();
		renderer.setAutoShapeType(true);
		renderer.setColor(Color.RED);
		
		player.addListener(view);
		
		TextureRegion blockTexture = TextureManager.INSTANCE.getTextureRegion(Gdx.files.classpath("com/felixcool98/tests/res/yellow.png"));
		
		for(int i = 0; i < 10; i++) {
			world.create(new Block(new Rectangle(i, -1, 1, 1), blockTexture));
		}
	}
	@Override
	public void renderGame() {
		world.step(GdxUtils.getDeltaTime());
		
		batch.begin();
		view.draw(batch);
		batch.end();
		
		float x = 0, y = 0;
		
		if(Gdx.input.isKeyPressed(Keys.W)) {
			y = 1;
		}
		if(Gdx.input.isKeyPressed(Keys.S)) {
			y = -1;
		}
		if(Gdx.input.isKeyPressed(Keys.A)) {
			x = -1;
		}
		if(Gdx.input.isKeyPressed(Keys.D)) {
			x = 1;
		}
		
		player.setSpeed(x, y);
	}
	
	
	private static class Entity extends MovingGameWorldObject implements GameWorldDrawable{
		private TextureRegion region;
		
		
		public Entity(Shape shape, TextureRegion region) {
			super(shape);
			
			this.region = region;
		}

		@Override
		public void draw(Batch batch) {
			batch.draw(region, getShape().getAABB().getLeft(), getShape().getAABB().getBot(),
					getShape().getAABB().getWidth(), getShape().getAABB().getHeight());
		}
		
		@EventListener
		public void collision(CollisionEvent event) {
			event.consume();
		}
	}
	private static class Block extends GameWorldObject implements GameWorldDrawable{
		private TextureRegion region;
		
		
		public Block(Shape shape, TextureRegion region) {
			super(shape);
			
			this.region = region;
		}

		@Override
		public void draw(Batch batch) {
			batch.draw(region, getShape().getAABB().getLeft(), getShape().getAABB().getBot(),
					getShape().getAABB().getWidth(), getShape().getAABB().getHeight());
		}
	}
}
