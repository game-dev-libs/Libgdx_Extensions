package gameworld;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.aabb.AABB;
import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.events.EventListener;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.GameWorldDrawable;
import com.felixcool98.gameworld.GameWorldObject;
import com.felixcool98.gameworld.draw.GameWorldView;
import com.felixcool98.gameworld.events.CreationEvent;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.gdxutility.textures.TextureManager;

public class GameWorldViewTest extends ApplicationAdapter {
	private GameWorld world;
	
	private GameWorldView view;
	
	private Batch batch;
	
	private ShapeRenderer renderer;
	
	
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		new Lwjgl3Application(new GameWorldViewTest(), config);
	}
	
	
	@Override
	public void create() {
		TextureRegion region = TextureManager.INSTANCE.getTextureRegion(Gdx.files.classpath("com/felixcool98/tests/res/blue.png"));
		
		world = new GameWorld();
		
		world.create(new TestDrawable(new Rectangle(0, 0, 1, 1), region));
		
		view = new GameWorldView(world, new OrthographicCamera(10, 10*GdxUtils.getAspectRatio()));
		
		view.getCamera().position.set(1, 1, 0);
		
		batch = new SpriteBatch();
		
		renderer = new ShapeRenderer();
		renderer.setAutoShapeType(true);
		renderer.setColor(Color.RED);
	}
	
	@Override
	public void render() {
		world.step(GdxUtils.getDeltaTime());
		
		renderer.begin();
		view.drawDebugGrid(renderer);
		renderer.end();
		
		batch.begin();
		
		view.draw(batch);
		
		batch.end();
	}
	
	
	private static class TestDrawable extends GameWorldObject implements GameWorldDrawable {
		private TextureRegion region;
		
		
		public TestDrawable(Shape shape, TextureRegion region) {
			super(shape);
			
			this.region = region;
		}

		@EventListener
		public void create(CreationEvent event) {
			System.out.println("created");
		}
		
		@Override
		public void draw(Batch batch) {
			if(region == null)
				return;
			
			AABB aabb = getShape().getAABB();
			
			batch.draw(region, aabb.getLeft(), aabb.getBot(),
					aabb.getRight()-aabb.getLeft(), aabb.getTop()-aabb.getBot());
		}
	}
}
