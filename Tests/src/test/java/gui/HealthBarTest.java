package gui;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.gui.HealthBar;

public class HealthBarTest extends ImprovedGameState {
	private HealthBar healthbar;
	
	
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("HealthBarTest");
		config.setWindowSizeLimits(1920, 1080, 1920, 1080);
		
		new Lwjgl3Application(new HealthBarTest(), config);
	}
	
	
	@Override
	public void create() {
		healthbar = new HealthBar(100, 10, false);
		healthbar.setValue(5);
		healthbar.setPosition(100, 100);
		getStage().addActor(healthbar);
	}
	
	
	@Override
	public void renderGame() {
		healthbar.addHP(GdxUtils.getDeltaTime()*10);
	}
}
