package gui;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.gui.LoadableScrollPane;
import com.felixcool98.widgets.Loadable;
import com.kotcrab.vis.ui.widget.VisImage;

public class LoadableScrollPaneTest extends ImprovedGameState {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setWindowSizeLimits(700, 500, 700, 500);
		config.setTitle("LoadableScrollPaneTest");
		
		new Lwjgl3Application(new LoadableScrollPaneTest(), config);
	}

	
	@Override
	public void create() {
		Table table = new Table();
		LoadableScrollPane pane = new LoadableScrollPane(table);
		pane.setSize(GdxUtils.getWidth(), GdxUtils.getHeight());
		
		Pixmap loadedPix = new Pixmap(25, 25, Format.RGBA8888);
		loadedPix.setColor(Color.GREEN);
		loadedPix.fill();
		
		Pixmap unloadedPix = new Pixmap(25, 25, Format.RGBA8888);
		unloadedPix.setColor(Color.RED);
		unloadedPix.fill();
		
		Texture loaded = new Texture(loadedPix);
		Texture unloaded = new Texture(unloadedPix);
		
		for(int y = 0; y < 100; y++) {
			for(int x = 0; x < 100; x++) {
				LoadableTest test = new LoadableTest(loaded, unloaded);
				
				table.add(test).size(loaded.getWidth(), loaded.getHeight());
			}
			
			table.row();
		}
		
		table.pack();
		
		getStage().addActor(pane);
	}
	
	
	private static class LoadableTest extends VisImage implements Loadable {
		private Texture loaded;
		private Texture unloaded;
		
		private boolean isLoaded = false;
		
		
		public LoadableTest(Texture loaded, Texture unloaded) {
			super();
			
			this.loaded = loaded;
			this.unloaded = unloaded;
			
			unload();
		}
		
		
		@Override
		public void load() {
			setDrawable(loaded);
			
			isLoaded = true;
		}

		@Override
		public void unload() {
			setDrawable(unloaded);
			
			isLoaded = false;
		}

		@Override
		public boolean isLoaded() {
			return isLoaded;
		}
	}
}
