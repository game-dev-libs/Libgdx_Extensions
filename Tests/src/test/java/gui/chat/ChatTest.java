package gui.chat;

import com.badlogic.gdx.utils.Align;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.gui.chat.Chat;
import com.felixcool98.gui.chat.ChatClient;
import com.felixcool98.gui.chat.ChatServer;

public class ChatTest extends ImprovedGameState {
	@Override
	public void create() {
		ChatServer server = new ChatServer();
		server.start(100);
		ChatClient client = new ChatClient("user");
		client.connect("192.168.178.52", 100);
		
		Chat chat = new Chat(200, 100, "Player2");
		chat.setPosition(100, 100);
		chat.align(Align.bottomLeft);
		chat.setClient(client);
		client.addMessageReceiver(chat);
		
		getStage().addActor(chat);
	}
}
