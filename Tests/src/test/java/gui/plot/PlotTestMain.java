package gui.plot;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class PlotTestMain {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("PlotTest");
		config.setWindowSizeLimits(1920, 1080, 1920, 1080);
		
		new Lwjgl3Application(new PlotTestImpl(), config);
	}
}
