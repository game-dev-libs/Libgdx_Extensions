package lighting;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.lighting.PixelPerfectLights;
import com.felixcool98.lighting.lightsources.PointLight;
import com.felixcool98.lighting.occluders.CircleOccluder;
import com.felixcool98.lighting.occluders.Occluder;
import com.felixcool98.lighting.occluders.RectangleOccluder;
import com.felixcool98.lighting.pixelperfect.PointLightHandler;

public class Test extends ImprovedGameState {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("Test");
		
		config.setWindowSizeLimits(700, 500, 700, 500);
		
		new Lwjgl3Application(new Test(), config);
	}
	
	
	private OrthographicCamera camera;
	private PixelPerfectLights lights;
	private Batch batch;
	private ShapeRenderer shape;
	private PointLight light;
	private List<Occluder> occluders = new LinkedList<>();
	
	
	@Override
	public void create() {
		setClearColor(Color.BLACK);
		
		disableAutoStageActAndRender();
		
		camera = new OrthographicCamera(1000, 1000 * GdxUtils.getAspectRatio());
		camera.translate(+camera.viewportWidth/2f, +camera.viewportHeight/2f);
		
		lights = new PixelPerfectLights();
		lights.register(new PointLightHandler());
		
		batch = new SpriteBatch();
		shape = new ShapeRenderer();
		shape.setAutoShapeType(true);
		
		light = new PointLight(500, 250, Color.PINK, 300);
		light.setDropOff(40);
		
		occluders.add(new RectangleOccluder(400, 250, 50, 100));
		occluders.add(new CircleOccluder(600, 250, 45));
	}
	@Override
	public void renderGame() {
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		shape.setProjectionMatrix(camera.combined);
		
		lights.update(light, occluders);
		
		FrameBuffer fbo = new FrameBuffer(Format.RGBA8888, (int)camera.viewportWidth, (int)camera.viewportHeight, false);
		
		fbo.begin();
		GdxUtils.clearScreen(0, 0, 0, 1);
		batch.begin();
		lights.render(light, batch);
		batch.end();
		fbo.end();
		
		clearScreen();
		
		batch.begin();
		for(Occluder o : occluders) {
			o.drawOcculuder(batch, shape);
		}
		batch.end();
		
		batch.begin();
		
		TextureRegion region = new TextureRegion(fbo.getColorBufferTexture());
		region.flip(false, true);
		
		batch.draw(region, camera.position.x-camera.viewportWidth/2f, camera.position.y-camera.viewportHeight/2f);
		batch.end();
		
		shape.begin();
		light.debugRender(shape);
		shape.end();
		
		
		
		lights.debugDraw(light, batch, shape);
	}
}
