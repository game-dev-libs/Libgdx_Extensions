package resources;

import static org.junit.Assert.*;

import org.junit.Test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Files;
import com.felixcool98.resources.Directory;
import com.felixcool98.resources.Resources;
import com.felixcool98.resources.SML;

public class DirectoryTests {

	@Test
	public void test() {
		Gdx.files = new Lwjgl3Files();
		
		Resources.registerResourceHandler(new SML.SMLResourceHandler());
		Resources.registerResourceHandler(new Directory.DirectoryResourceHandler());
		
		Directory directory = Resources.getDirectory(Gdx.files.local(""));
		
		assertEquals(1, directory.getSMLs("mod").size());
		
		assertEquals(3, directory.getSMLs("mod").get(0).getTypes("block").size());
	}

}
