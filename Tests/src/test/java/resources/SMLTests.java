package resources;

import static org.junit.Assert.*;

import org.junit.Test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Files;
import com.felixcool98.resources.Resources;
import com.felixcool98.resources.SML;

public class SMLTests {

	@Test
	public void test() {
		Gdx.files = new Lwjgl3Files();
		
		Resources.registerResourceHandler(new SML.SMLResourceHandler());
		
		SML sml = Resources.getSML(Gdx.files.local("blocks.mod"));
		
		assertEquals(3, sml.getTypes("block").size());
		assertEquals(1, sml.getTypes("other").size());
	}

}
