package skeleton.animations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.gdxutility.InputUtils;
import com.felixcool98.skeleton.TimeLine;
import com.felixcool98.skeleton.TimeLine.FrameChangeListener;
import com.felixcool98.skeleton.image.ImageSkeletalAnimation;
import com.felixcool98.skeleton.image.ImageSkeletonBoneEditor;
import com.felixcool98.skeleton.image.ImageSkeletonBuilder;
import com.felixcool98.skeleton.image.ImageSkeletonInstance;

public class Impl extends ImprovedGameState {
	private float time;
	
	private ImageSkeletalAnimation animation;
	
	private TimeLine timeLine;
	
	private boolean rightPressed = false;
	private boolean leftPressed = false;
	
	private int step;
	
	private ImageSkeletonBoneEditor editor;
	
	
	@Override
	public void create() {
		ImageSkeletonInstance skeleton = new ImageSkeletonBuilder()
				.addBone("bone")
				.setImage("tests/skeleton/Arrow.png")
				.setData(90, 100, 0)
				.setImageData(-10, 0, 20, 100, -90)
				.setDepth(1)
				
				.addBone("bone2")
				.setImage("tests/skeleton/Arrow.png")
				.setData(90, 50, 100)
				.setImageData(-15, 10, 30, 20, -90)
				
				.setParent("bone")
				
				.addBone("bone3")
				.setImage("tests/skeleton/Arrow.png")
				.setData(-90, 50, 100)
				.setImageData(-15, 10, 30, 20, -90)
				
				.build();
		
		animation = new ImageSkeletalAnimation(skeleton);
		animation.addAngleToBone("bone2", 90, 1, 0);
		animation.addAngleToBone("bone2", -90, 1, 1);
		

		Table table = new Table();{
			table.setSize(getStage().getWidth(), getStage().getHeight());
			
			editor = new ImageSkeletonBoneEditor();{
				editor.setSize(100, 300);
				editor.setPosition(0, getStage().getHeight(), Align.topLeft);
			}table.add(editor).left().top();
			
			table.row();
			
			timeLine = new TimeLine();{
				timeLine.setPosition(100, 100, Align.bottomLeft);
				timeLine.setFrameOffSet(0);
				timeLine.setCurrentFrame(60);
				timeLine.setFps(4);
				timeLine.setPixelPerFps(20);
				timeLine.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						timeLine.setCurrentFrame(timeLine.getFrameAt(x));
						getStage().setKeyboardFocus(timeLine);
						super.clicked(event, x, y);
					}
					@Override
					public boolean mouseMoved(InputEvent event, float x, float y) {
						return super.mouseMoved(event, x, y);
					}
				});
				timeLine.addListener(new FrameChangeListener() {
					@Override
					public void frameChanged(int frame, float time) {
						Impl.this.time = time;
					}
				});
				timeLine.addListener(new InputListener() {
					@Override
					public boolean keyDown(InputEvent event, int keycode) {
						if(keycode == Keys.RIGHT)
							rightPressed = true;
						if(keycode == Keys.LEFT)
							leftPressed = true;
						
						return super.keyDown(event, keycode);
					}
					@Override
					public boolean keyUp(InputEvent event, int keycode) {
						if(keycode == Keys.RIGHT)
							rightPressed = false;
						if(keycode == Keys.LEFT)
							leftPressed = false;
						
						return super.keyUp(event, keycode);
					}
				});
			}table.add(timeLine).size(400, 60).expandX().expandY().bottom();
		}getStage().addActor(table);
		
		table.debug();
	}
	@Override
	public void renderGame() {
		animation.render(new SpriteBatch(), 300, 300, 0, time);
		animation.getInstanceAtTime(time).renderDebugLines(new ShapeRenderer(), 300, 300, 0);
		
		if(Gdx.input.isButtonPressed(Buttons.LEFT)&&
				animation.getInstanceAtTime(time).getBoneAtPos(InputUtils.getMouseX(), GdxUtils.getHeight()-InputUtils.getMouseY(), 300, 300) != null)
			editor.setBone(animation.getInstanceAtTime(time).getBoneAtPos(InputUtils.getMouseX(), GdxUtils.getHeight()-InputUtils.getMouseY(), 300, 300));
		
		step ++;
		
		if(step % 5 == 0) {
			if(leftPressed)
				timeLine.addToCurrentFrame(-1);
			if(rightPressed)
				timeLine.addToCurrentFrame(1);
		}
	}
}
