package widgets.imageeditor.canvas;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.widgets.imageeditor.canvas.DrawableCanvas;
import com.kotcrab.vis.ui.building.utilities.Alignment;

public class DrawableCanvasTest extends ImprovedGameState {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("DrawableCanvasTest");
		config.setWindowSizeLimits(1920, 1080, 1920, 1080);
		
		new Lwjgl3Application(new DrawableCanvasTest(), config);
	}
	
	
	@Override
	public void created() {
		Table table = new Table();{
			table.align(Align.bottomLeft);
			
			table.add().width(50);
			
			DrawableCanvas canvas = new DrawableCanvas();{
//				canvas.setEditorAction(new EditorAction.Move());
				canvas.setSize(300, 300);
				canvas.newImage(16, 16);
				canvas.setDrawColor(Color.RED);
				canvas.drawRectFilled(2, 2, 8, 8);
				canvas.align(Align.bottomLeft);
				
				canvas.resizeImage(32, 32, Alignment.CENTER, 2);
			}table.add(canvas).size(300, 300).align(Align.bottomLeft);
		}getStage().addActor(table);
		
		getStage().setDebugAll(true);
	}
}
