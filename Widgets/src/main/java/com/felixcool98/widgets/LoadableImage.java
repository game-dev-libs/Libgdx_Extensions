package com.felixcool98.widgets;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.widget.VisImage;

public class LoadableImage extends VisImage implements Loadable {
	private AssetManager manager;
	private String path;
	
	private boolean loaded = false;
	private boolean loadRequested = false;
	private boolean error = false;
	
	private int updateTime = 1;
	
	
	public LoadableImage(String path) {
		this(path, new AssetManager());
	}
	public LoadableImage(String path, AssetManager manager) {
		setPath(path);
		this.manager = manager;
	}

	
	public void setPath(String path) {
		this.path = path.replaceAll("\\\\", "/");
	}
	public void setUpdateTime(int time) {
		updateTime = time;
	}
	
	
	public String getPath() {
		return path;
	}
	public AssetManager getManager() {
		return manager;
	}
	

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if(!error && loadRequested)
			updateManager();
		
		super.draw(batch, parentAlpha);
	}
	private void updateManager() {
		if(!manager.isLoaded(path)) {
			try {
				manager.update(updateTime);
			}catch (Exception e) {
				e.printStackTrace();
				
				error = true;
			}
		}else {
			setDrawable((Texture) manager.get(path));
			
			loadRequested = false;
		}
	}
	
	public void load() {
		if(isLoaded() || error)
			return;
		
		if(!manager.isLoaded(path)) {
			manager.load(path, Texture.class);
			
			loadRequested = true;
		}
		
		loaded = true;
	}
	public void unload() {
		if(!isLoaded())
			return;
		
		loaded = false;
		
		if(!error && !loadRequested) {
			if(manager.isLoaded(path))
				manager.unload(path);
		}
		
		setDrawable((Drawable) null);
	}
	
	public boolean isLoaded() {
		return loaded;
	}
}
