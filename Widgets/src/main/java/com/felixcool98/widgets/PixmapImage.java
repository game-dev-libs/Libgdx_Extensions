package com.felixcool98.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

/**
 * an Actor encapsulating a pixmap<br>
 * most of the methods of the pixmap can be called on this actor<br>
 * <br>
 * part of code copied from {@link Image}
 * 
 * @author felixcool98
 */
public class PixmapImage extends Widget {
	private Scaling scaling;
	private int align = Align.center;
	private float imageX, imageY, imageWidth, imageHeight;
	private Pixmap pixmap;
	private TextureRegion pixmapTexture;
	
	private Drawable background;


	/**
	 * creates a new pixmapImage with a new pixmap with the given width and height and Format.RGBA8888 <br>
	 * Scaling will be Scaling.fit and align will be Aling.center
	 * 
	 * @param width if <= 0 will be replaced with 64 
	 * @param height if <= 0 will be replaced with 64 
	 */
	public PixmapImage(int width, int height) {
		this(new Pixmap(width > 0 ? width : 64, height > 0 ? height : 64, Format.RGBA8888));
	}
	/**
	 * creates a new pixmap image with the given pixmap<br>
	 * Scaling will be Scaling.fit and align will be Aling.center
	 * 
	 * @param pixmap not null, otherwise it will create a new pixmap with size 64x64 and Format.RGBA8888
	 */
	public PixmapImage(Pixmap pixmap) {
		this(pixmap, Scaling.fit, Align.center);
	}
	/**
	 * 
	 * @param pixmap not null, otherwise it will create a new pixmap with size 64x64 and Format.RGBA8888
	 * @param scaling the scaling to use
	 * @param align the align to use
	 */
	public PixmapImage(Pixmap pixmap, Scaling scaling, int align) {
		setPixmap(pixmap);
		
		this.scaling = scaling;
		this.align = align;
		
		setSize(getPrefWidth(), getPrefHeight());
	}
	
	
	//======================================================================
	// setter
	//======================================================================
	/**
	 * calls setPixmap(pixmap, true)
	 */
	public void setPixmap(Pixmap pixmap) {
		setPixmap(pixmap, true);
	}
	/**
	 * sets the pixmap of the image
	 * 
	 * @param pixmap if null will create a new pixmap with size 64x64 and Format.RGBA8888
	 * @param dispose if true disposes the current pixmap
	 */
	public void setPixmap(Pixmap pixmap, boolean dispose) {
		if (this.pixmap == pixmap) 
			return;
		
		if(dispose && this.pixmap != null) 
			this.pixmap.dispose();
		
		if(pixmap == null)
			pixmap = new Pixmap(64, 64, Format.RGBA8888);
		
		if (getPrefWidth() != pixmap.getWidth() || getPrefHeight() != pixmap.getHeight()) 
			invalidateHierarchy();
		
		this.pixmap = pixmap;
	}
	
	public void setBackground(Drawable drawable) {
		background = drawable;
	}
	
	public void setScaling (Scaling scaling) {
		if (scaling == null) 
			throw new IllegalArgumentException("scaling cannot be null.");
		
		this.scaling = scaling;
		
		invalidate();
	}

	public void setAlign (int align) {
		this.align = align;
		
		invalidate();
	}
	

	@Override
	public void layout () {
		if (pixmap == null)
			return;
		
		if(pixmapTexture != null)
			pixmapTexture.getTexture().dispose();
		
		pixmapTexture = new TextureRegion(new Texture(pixmap));
		pixmapTexture.flip(false, true);

		float regionWidth = pixmap.getWidth();
		float regionHeight = pixmap.getHeight();
		float width = getWidth();
		float height = getHeight();

		Vector2 size = scaling.apply(regionWidth, regionHeight, width, height);
		imageWidth = size.x;
		imageHeight = size.y;

		if ((align & Align.left) != 0)
			imageX = 0;
		else if ((align & Align.right) != 0)
			imageX = (int)(width - imageWidth);
		else
			imageX = (int)(width / 2 - imageWidth / 2);

		if ((align & Align.top) != 0)
			imageY = (int)(height - imageHeight);
		else if ((align & Align.bottom) != 0)
			imageY = 0;
		else
			imageY = (int)(height / 2 - imageHeight / 2);
	}
	@Override
	public void draw(Batch batch, float parentAlpha) {
		validate();

		Color color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
		
		drawBackground(batch);

		float x = getX();
		float y = getY();
		float scaleX = getScaleX();
		float scaleY = getScaleY();
		float rotation = getRotation();
		
		batch.draw(pixmapTexture, x+imageX, y+imageY, getOriginX()-imageX, getOriginY()-imageY, imageWidth, imageHeight, scaleX, scaleY, rotation);
	}
	protected void drawBackground(Batch batch) {
		if(background == null)
			return;
		
		float backgroundWidth = background.getMinWidth();
		float backgroundHeight = background.getMinHeight();
		
		float iSteps = getWidth()/background.getMinWidth();
		float jSteps = getHeight()/background.getMinHeight();
		
		for(float i = 0; i < iSteps; i++) {
			for(int j = 0; j < jSteps; j++) {
				if(i >= iSteps-1 && j >= jSteps-1) {
					float width = getWidth()-backgroundWidth * i;
					float height = getHeight()-backgroundHeight * j;
					
					background.draw(batch, getX()+i*backgroundWidth, getY()+j*backgroundHeight, width, height);
				}else if(i >= iSteps-1) {
					float width = getWidth()-backgroundWidth * i;
					
					background.draw(batch, getX()+i*backgroundWidth, getY()+j*backgroundHeight, width, backgroundHeight);
				}else if(j >= jSteps-1) {
					float height = getHeight()-backgroundHeight * j;
					
					background.draw(batch, getX()+i*backgroundWidth, getY()+j*backgroundHeight, backgroundWidth, height);
				}else {
					background.draw(batch, getX()+i*backgroundWidth, getY()+j*backgroundHeight, backgroundWidth, backgroundHeight);
				}
			}
		}
	}


	/**
	 * 
	 * @return never null
	 */
	public Pixmap getPixmap () {
		return pixmap;
	}

	public float getMinWidth () {
		return 0;
	}

	public float getMinHeight () {
		return 0;
	}

	public float getPrefWidth () {
		if(pixmap == null)
			return 0;
			
		return pixmap.getWidth();
	}

	public float getPrefHeight () {
		if(pixmap == null)
			return 0;
		
		return pixmap.getHeight();
	}

	public float getImageX () {
		return imageX;
	}

	public float getImageY () {
		return imageY;
	}

	public float getImageWidth () {
		return imageWidth;
	}

	public float getImageHeight () {
		return imageHeight;
	}
	
	
	//======================================================================
	// change pixmap
	//======================================================================
	/**
	 * calls new Pixmap(width, height, Format.RGBA8888)
	 * 
	 * @param width
	 * @param height
	 */
	public void newPixmap(int width, int height) {
		newPixmap(width, height, Format.RGBA8888);
	}
	/**
	 * sets the pixmap to a new pixmap created with the given values
	 * 
	 * @param width the width of the pixmap in pixels
	 * @param height the height of the pixmap in pixels
	 * @param format the format of the pixmap
	 */
	public void newPixmap(int width, int height, Format format) {
		setPixmap(new Pixmap(width, height, format));
	}
	
	
	//======================================================================
	// Pixmap methods
	//======================================================================
	/**{@link Pixmap#drawPixel(int, int)}**/
	public void pixmapDrawPixel(int x, int y) {
		pixmap.drawPixel(x, y);
		
		invalidate();
	}
	
	/**{@link Pixmap#drawLine(int, int, int, int)}**/
	public void pixmapDrawLine(int x, int y, int x2, int y2) {
		pixmap.drawLine(x, y, x2, y2);
		
		invalidate();
	}
	
	/**{@link Pixmap#drawCircle(int, int, int)}**/
	public void pixmapDrawCircle(int x, int y, int radius) {
		pixmap.drawCircle(x, y, radius);
		
		invalidate();
	}
	
	/**{@link Pixmap#drawRectangle(int, int, int, int)}**/
	public void pixmapDrawRectangle(int x, int y, int width, int height) {
		pixmap.drawRectangle(x, y, width, height);
		
		invalidate();
	}
	
	/**{@link Pixmap#fill()}**/
	public void pixmapFill() {
		pixmap.fill();
		
		invalidate();
	}
	
	//setter
	
	/**{@link Pixmap#setColor(Color)}**/
	public void pixmapSetColor(Color color) {
		pixmap.setColor(color);
	}
	
	/**{@link Pixmap#setBlending(Blending)}**/
	public void pixmapSetBlending(Blending blending) {
		pixmap.setBlending(blending);
	}
	
	//getter
	
	/**{@link Pixmap#getPixel(int, int)}**/
	public int pixmapGetPixel(int x, int y) {
		return pixmap.getPixel(x, y);
	}
	
	/**{@link Pixmap#getWidth()}**/
	public int getPixmapWidth() {
		return pixmap.getWidth();
	}
	
	/**{@link Pixmap#getHeight()}**/
	public int getPixmapHeight() {
		return pixmap.getHeight();
	}
	
	/**{@link Pixmap#getFormat()}**/
	public Format getPixmapFormat() {
		return pixmap.getFormat();
	}
}
