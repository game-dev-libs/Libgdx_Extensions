package com.felixcool98.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class ScaleableTable extends Table {
	@Override
	public void scaleBy(float scale) {
		super.scaleBy(scale);
		invalidateHierarchy();
	}
	@Override
	public void setScale(float scaleX, float scaleY) {
		super.setScale(scaleX, scaleY);
		invalidateHierarchy();
	}
	@Override
	public void setScale(float scaleXY) {
		super.setScale(scaleXY);
		invalidateHierarchy();
	}
	
	@Override
	public float getMinWidth() {
		return super.getMinWidth()*getScaleX();
	}
	@Override
	public float getMinHeight() {
		return super.getMinHeight()*getScaleY();
	} 
	@Override
	public float getPrefWidth() {
		return super.getPrefWidth()*getScaleX();
	}
	@Override
	public float getPrefHeight() {
		return super.getPrefHeight()*getScaleY();
	}
}
