package com.felixcool98.widgets.html;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class Attributes {
	public static void process(String tag, Table actorTable, Actor actor) {
		if(tag.contains("style")) {
			int stylePos = tag.indexOf("style") + 7;
			int styleLength = tag.substring(stylePos).indexOf("\"");
			
			String style = tag.substring(stylePos, stylePos + styleLength);
			
			processStyle(style, actor, actorTable);
		}
	}
	
	
	private static void processStyle(String style, Actor actor, Table actorTable) {
		style = style.replaceAll("\\s+", "");
		
		String[] styles = style.split(";");
		
		if(actor instanceof Label) {
			for(String st : styles) {
				if(st.contains("background-color")) {
					String colorText = st.replace("background-color:", "");
					
					Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
					pixmap.setColor(getColor(colorText));
					pixmap.fill();
					
					actorTable.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(pixmap))));
					
					pixmap.dispose();
				}else if(st.contains("color")) {
					String colorText = st.replace("color:", "");
					
					actor.setColor(getColor(colorText));
				}
			}
		}
	}
	
	
	private static Color getColor(String colorText) {
		Color color;
		
		if(colorText.startsWith("#")) {
			colorText = colorText.substring(1);
			
			int r = Integer.parseInt(colorText.substring(0, 2), 16);
			int g = Integer.parseInt(colorText.substring(2, 4), 16);
			int b = Integer.parseInt(colorText.substring(4, 6), 16);
			
			color = new Color(r / 256f, g / 256f, b / 256f, 1);
		}else {
			color = Color.BLACK;
		}
		
		return color;
	}
}
