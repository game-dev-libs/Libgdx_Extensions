package com.felixcool98.widgets.html;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;

public class Element implements IElement {
	private Skin skin;
	
	private String rest;
	
	private Table table;
	
	
	public Element(String text, Skin skin) {
		this.skin = skin;
		
		process(text);
	}
	
	
	private void process(String text) {
		table = new Table();
		
		String element;
		
		if(text.contains("<"))
			element = text.substring(0, text.indexOf("<"));
		else 
			element = text;
		
		Label label = new VisLabel(element, skin.get(LabelStyle.class));
		
		table.add(label).align(Align.topLeft);
		
		if(text.contains("<"))
			rest = text.substring(text.indexOf("<"));
		else
			rest = "";
	}
	
	
	@Override
	public String getRest() {
		return rest;
	}
	@Override
	public Table getTable() {
		return table;
	}
	
	
	public static class ElementBuilder implements IElementBuilder<Element> {
		@Override
		public Element createElement(String text, Skin skin) {
			return new Element(text, skin);
		}

		@Override
		public boolean valid(String text) {
			return true;
		}
		
	}
}
