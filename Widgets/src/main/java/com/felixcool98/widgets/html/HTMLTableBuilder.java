package com.felixcool98.widgets.html;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.felixcool98.widgets.html.IElement.IElementBuilder;
import com.kotcrab.vis.ui.VisUI;

/**
 * <h1>supported tags: </h1>
 * &ltbr&gt linebreak <br>
 * &ltp&gt paragraph <br>
 * <br>
 * <h1>attributes: </h1>
 * <h1>style: </h1>
 * color <br>
 * background-color <br>
 * <br>
 * <h1>colors: </h1>
 * only hex
 * 
 * @author felixcool98
 *
 */
public class HTMLTableBuilder {
	private Skin skin;
	
	private String text;
	
	private Table table;
	
	private List<IElementBuilder<?>> elementBuilder = new LinkedList<>();
	
	
	public HTMLTableBuilder(String text) {
		this(VisUI.getSkin(), text);
	}
	public HTMLTableBuilder(Skin skin, String text) {
		this.skin = skin;
		
		this.text = text;
		
		addElementBuilder();
	}
	
	
	protected void addElementBuilder() {
		elementBuilder.add(new Paragraph.ParagraphBuilder());
		
		elementBuilder.add(new Element.ElementBuilder());
	}
	
	
	public Table build() {
		table = new Table();
		
		text = text.replaceAll("<br>", "\n");
		
		while(text.length() > 0) {
			for(IElementBuilder<?> builder: elementBuilder) {
				if(!builder.valid(text))
					continue;
				
				IElement element = builder.createElement(text, skin);
				
				text = element.getRest();
				
				table.add(element.getTable()).align(Align.topLeft);
				
				table.row();
				
				break;
			}
		}
		
		return table;
	}
}
