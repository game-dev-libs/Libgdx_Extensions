package com.felixcool98.widgets.html;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public interface IElement {
	public String getRest();
	
	public Table getTable();
	
	
	public static interface IElementBuilder<T extends IElement> {
		public T createElement(String text, Skin skin);
		
		public boolean valid(String text);
	}
}
