package com.felixcool98.widgets.imageeditor;

import java.util.List;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.felixcool98.widgets.imageeditor.canvas.DrawableCanvas;

public class EditorToolBar extends Table {
	private EditorToolBarStyle style;
	private DrawableCanvas canvas;
	
	
	public void setEditorTools(List<EditorTool> tools) {
		setEditorTools(tools.toArray(new EditorTool[0]));
	}
	public void setEditorTools(EditorTool[] tools) {
		int i = 0;
		
		while(i < tools.length) {
			for(int x = 0; x < style.rowSize && i != tools.length; x++) {
				if(tools[i] == null) {
					i++;
					break;
				}
				
				EditorTool tool = tools[i];
				
				if(tool != null) {
					tool.setCanvas(this.canvas);
					
					this.add(tool).size(style.fieldSize).pad(style.fieldPad);
				}else {
					this.add().size(style.fieldSize).pad(style.fieldPad);
				}
				
				i++;
			}
			
			if(i != tools.length)
				this.row();
		}
	}
	
	
	//================================================================================================================================================
	// setter
	//================================================================================================================================================
	public void setStyle(EditorToolBarStyle style) {
		this.style = style;
	}
	public void setCanvas(DrawableCanvas canvas) {
		this.canvas = canvas;
	}
	
	
	public static class EditorToolBarStyle {
		public int rowSize;
		public float fieldSize;
		public float fieldPad;
	}
}
