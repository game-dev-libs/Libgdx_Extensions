package com.felixcool98.gml;

import java.util.HashMap;
import java.util.Map;

import com.felixcool98.gml.objects.GMLObject;
import com.felixcool98.gml.scripts.Script;

public class Scope {
	private Map<String, GMLObject> objects = new HashMap<>();
	private Map<String, Script> scripts = new HashMap<>();
	
	
	/**
	 * creates a new object by parsing it and adds it to the scope
	 * 
	 * @param name
	 * @param parse
	 */
	public void add(String name, String parse) {
		add(name, GMLObject.parseObject(parse));
	}
	public void add(String name, Object object) {
		add(name, new GMLObject(object));
	}
	public void add(String name, GMLObject object) {
		objects.put(name, object);
	}
	
	public void add(String name, Script script) {
		scripts.put(name, script);
	}
	
	
	public GMLObject get(String name) {
		return objects.get(name);
	}
	public Script getScript(String name) {
		return scripts.get(name);
	}
	
	
	public boolean contains(String name) {
		return objects.containsKey(name);
	}
	
	
	@Override
	public String toString() {
		return objects.toString();
	}
	
	
	public static Scope getScope(Scope global, Scope object, Scope local, ScopeType type, String name) {
		if(type == ScopeType.AUTO) {
			if(name.startsWith("global.")) {
				return global;
			}else {
				if(local != null && local.contains(name))
					return local;
				else
					return object;
			}
		}else {
			switch (type) {
				case GLOBAL:
					return global;
				case OBJECT:
					return object;
				case LOCAL:
					return local;
	
				default:
					break;
			}
		}
		
		return null;
	}
	
	
	public static enum ScopeType {
		GLOBAL,
		OBJECT,
		LOCAL,
		AUTO;
	}
}
