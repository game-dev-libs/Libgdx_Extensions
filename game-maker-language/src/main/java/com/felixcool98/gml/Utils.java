package com.felixcool98.gml;

public class Utils {
	public static boolean isNumber(String text) {
		try {
			Integer.parseInt(text);
		}catch (Exception e) {
			try {
				Float.parseFloat(text);
			} catch (Exception e2) {
				return false;
			}
		}
		
		return true;
	}
	public static Object toNumber(String text) {
		try {
			return Integer.parseInt(text);
		} catch (Exception e) {
			return Float.parseFloat(text);
		}
	}
}
