package com.felixcool98.gml.maths;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.objects.GMLObject;

public interface MathFunction {
	public GMLObject get(Scope global, Scope object, Scope local);
}
