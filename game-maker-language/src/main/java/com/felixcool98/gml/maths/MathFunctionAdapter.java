package com.felixcool98.gml.maths;

public abstract class MathFunctionAdapter implements MathFunction {
	protected MathFunction f1;
	protected MathFunction f2;
	

	public MathFunctionAdapter(MathFunction f1, MathFunction f2) {
		if(f1 == null)
			throw new IllegalArgumentException("f1 can't be null");
		if(f2 == null)
			throw new IllegalArgumentException("f2 can't be null");
		
		this.f1 = f1;
		this.f2 = f2;
	}
	
	
	@Override
	public String toString() {
		return "("+f1.toString()+"+"+f2.toString()+")";
	}
}
