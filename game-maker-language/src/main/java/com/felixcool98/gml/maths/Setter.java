package com.felixcool98.gml.maths;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.Scope.ScopeType;
import com.felixcool98.gml.objects.GMLObject;

public class Setter {
	private String name;
	private ScopeType scope;
	
	private MathFunction to;
	
	
	public Setter(String name, MathFunction to) {
		this.name = name.replaceFirst("global.", "");
		this.scope = name.startsWith("global.") ? ScopeType.GLOBAL : null;
		this.to = to;
	}
	
	
	public void set(Scope global, Scope object, Scope local) {
		Scope scope = null;
		
		if(this.scope == ScopeType.GLOBAL) {
			scope = global;
		}else {
			if(local.contains(name))
				scope = local;
			else
				scope = object;
		}
		
		if(!scope.contains(name)) {
			scope.add(name, new GMLObject());
		}
		
		scope.get(name).setValue(to.get(global, object, local).getValue());
	}
}
