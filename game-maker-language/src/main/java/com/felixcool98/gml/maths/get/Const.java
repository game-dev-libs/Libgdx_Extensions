package com.felixcool98.gml.maths.get;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.maths.MathFunction;
import com.felixcool98.gml.objects.GMLObject;

public class Const implements MathFunction {
	private GMLObject constant;
	
	
	public Const(GMLObject constant) {
		this.constant = constant;
	}
	
	
	@Override
	public GMLObject get(Scope global, Scope object, Scope local) {
		return constant;
	}
	
	
	@Override
	public String toString() {
		return constant.getValue().toString();
	}
}
