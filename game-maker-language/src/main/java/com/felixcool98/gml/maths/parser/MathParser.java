package com.felixcool98.gml.maths.parser;

import com.felixcool98.gml.Utils;
import com.felixcool98.gml.maths.MathFunction;
import com.felixcool98.gml.maths.basic.BasicMath;
import com.felixcool98.gml.maths.get.Const;
import com.felixcool98.gml.maths.get.ScriptGetter;
import com.felixcool98.gml.maths.get.VarGetter;
import com.felixcool98.gml.objects.GMLObject;

public class MathParser {
	public MathFunction parse(String text) {
		MathFunction function = null;
		MinInstruction sign = null;
		
		MinInstruction[] instructions = new MinInstructionParser().parse(text);
		
		for(int i = 0; i < instructions.length; i++) {
			MinInstruction instruction = instructions[i];
			
			if(instruction.isSign()) {
				sign  = instruction;
			}else {
				if(function == null)
					function = getFunction(instruction);
				else {
					function = createFunction(function, sign, getFunction(instruction));
				}
			}
		}
		
		return function;
	}
	
	
	private MathFunction createFunction(MathFunction f1, MinInstruction sign, MathFunction f2) {
		if(sign == null) {
			return getFunction(new MinInstruction(((VarGetter) f1).getName()+"("+((VarGetter) f2).getName()+")"));
		}
		
		if(BasicMath.isBasic(sign.getText()))
			return BasicMath.createBasic(f1, sign.getText(), f2);
		
		return null;
	}
	private MathFunction getFunction(MinInstruction instruction) {
		if(!instruction.hasChildren()) {
			if(instruction.isFunctionCall()) {
				String argumentString = instruction.getText().substring(instruction.getText().indexOf("("), instruction.getText().indexOf(")"));
				
				return new ScriptGetter(instruction.getText().substring(0, instruction.getText().indexOf("(")), argumentString.split(","));
			}else if(Utils.isNumber(instruction.getText()) || instruction.getText().startsWith("\"") && instruction.getText().endsWith("\"")) {
				return new Const(GMLObject.parseObject(instruction.getText()));
			}else {
				return new VarGetter(instruction.getText());
			}
		}else {
			MathFunction function = null;
			MinInstruction sign = null;
			
			for(MinInstruction child : instruction.getChildren()) {
				System.err.println(child.toString());
				if(child.isSign()) {
					sign  = child;
				}else {
					if(function == null)
						function = getFunction(child);
					else {
						function = createFunction(function, sign, getFunction(child));
					}
				}
			}
			
			return function;
		}
	}
}
