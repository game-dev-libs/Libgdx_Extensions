package com.felixcool98.gml.maths.parser;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.gml.maths.basic.BasicMath;

public class MinInstruction {
	private String text;
	private List<MinInstruction> children = new LinkedList<>();
	private MinInstruction parent;
	
	
	public MinInstruction() {
		this("");
	}
	public MinInstruction(String text) {
		if(text == null)
			throw new IllegalArgumentException("text can't be null");
		
		this.text = text.trim();
	}
	
	
	public void add(MinInstruction instruction) {
		if(instruction.children.isEmpty() && instruction.getText().isEmpty())
			return;
		
		children.add(instruction);
	}
	public void add(String text) {
		this.text += text;
	}
	
	
	public void setParent(MinInstruction instruction) {
		parent = instruction;
	}
	
	
	//======================================================================
	// checks
	//======================================================================
	public boolean startsParent() {
		return text.equals("(");
	}
	public boolean endsParent() {
		return text.equals(")");
	}
	
	public boolean hasParent() {
		return parent != null;
	}
	public boolean hasChildren() {
		return !children.isEmpty();
	}
	
	public boolean isSign() {
		if(hasChildren()) 
			return false;
		
		return BasicMath.isBasic(getText());
	}
	public boolean isFunctionCall() {
		return !getText().startsWith("(") && getText().endsWith(")") && getText().contains("(");
	}
	
	public boolean isEmpty() {
		return getText().isEmpty() && getChildren().isEmpty();
	}
	
	
	public String getText() {
		return text;
	}
	public MinInstruction getParent() {
		return parent;
	}
	public List<MinInstruction> getChildren(){
		return children;
	}
	
	
	public void fixChildren() {
		if(!hasChildren())
			return;
		
		for(MinInstruction child : children) {
			child.fixChildren();
		}
		
		if(children.size() == 1) {
			if(hasParent()) {
				parent.children.remove(this);
				parent.add(children.get(0));
			}else if(!children.get(0).hasChildren()) {
				text = children.get(0).getText();
				children.clear();
			}
		}
	}
	
	
	@Override
	public String toString() {
		String str = "[" + text;
		if(!children.isEmpty())
			str += " " + children.toString();
		
		
		return str+"]";
	}
	public String simpleString() {
		String str = text;
		if(!children.isEmpty()){
			str += "(";
			for(MinInstruction child : children) {
				str += child.simpleString();
			}
			str += ")";
		}
		
		
		return str;
	}
}
