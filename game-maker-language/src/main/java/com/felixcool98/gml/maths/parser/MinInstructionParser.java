package com.felixcool98.gml.maths.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class MinInstructionParser {
	private boolean string = false;
	
	private List<MinInstruction> instructions = new LinkedList<>();
	
	private String current = "";
	
	
	public MinInstruction[] parse(String text) {
		string = false;
		current = "";
		instructions = new LinkedList<>();
		
		for(int i = 0; i < text.length(); i++) {
			evaluate(text.charAt(i));
		}
		
		push(current);
		
		processMinInstructions();
		postProcessMinInstructions();
		fixChildren();
		
		return instructions.toArray(new MinInstruction[0]);
	}
	
	
	private void processMinInstructions() {
		List<MinInstruction> instructions = new ArrayList<MinInstruction>(this.instructions);
		this.instructions.clear();
		
		MinInstruction parent = null;
		
		for(MinInstruction instruction : instructions) {
			if(instruction.startsParent()) {
				if(parent != null) {
					MinInstruction child = new MinInstruction();
					
					parent.add(child);
					child.setParent(parent);
					
					parent = child;
				}else {
					parent = new MinInstruction();
					this.instructions.add(parent);
				}
				continue;
			}
			if(parent != null && instruction.endsParent() && !instruction.isFunctionCall()) {
				parent = parent.getParent();
				
				continue;
			}
			
			if(parent != null) {
				parent.add(instruction);
				instruction.setParent(parent);
			}else
				addToInstructions(instruction);
		}
	}
	
	private void postProcessMinInstructions() {
		List<MinInstruction> instructions = new ArrayList<MinInstruction>(this.instructions);
		this.instructions.clear();
		//TODO  if multiply or divide is between 2 mininstructions combine them into one
		
		for(int i = 1; i < instructions.size()-1; i+=2) {//loop only through signs
			MinInstruction previous = instructions.get(i-1);
			MinInstruction current = instructions.get(i);
			MinInstruction next = instructions.get(i+1);
			
			//if multiplication or division combine
			//else add previous and current to instructions
			if(current.getText().equals("*") || current.getText().equals("/")) {
				MinInstruction parent = new MinInstruction();
				
				addToParent(previous, parent);
				addToParent(current, parent);
				addToParent(next, parent);

				instructions.remove(i - 1);
				instructions.remove(i - 1);
				instructions.remove(i - 1);
				
				instructions.add(i - 1, parent);
				System.out.println();
				print(instructions);
				System.out.println();
				i -= 2;
			}
		}
		
		addToInstructions(instructions);
	}
	private void fixChildren() {
		for(MinInstruction instruction : instructions) {
			instruction.fixChildren();
		}
	}
	
	private void print(List<MinInstruction> instructions) {
		for(MinInstruction instruction : instructions) {
			System.out.print(instruction.simpleString());
		}
	}
	
	private void evaluate(char c) {
		if(c == '"') {
			push(current);
			current = "";
			string = !string;
		}
		if(string) {
			current += c;
			return;
		}
		
		if(c == '+') {
			push(current);
			push("+");
			
			current = "";
			
			return;
		}
		if(c == '-') {
			push(current);
			push("-");
			
			current = "";
			
			return;
		}
		if(c == '*') {
			push(current);
			push("*");
			
			current = "";
			
			return;
		}
		if(c == '/') {
			push(current);
			push("/");
			
			current = "";
			
			return;
		}
		if(c == '(' && !isFunctionCall(current)) {
			push(current);
			push("(");
			
			current = "";
			
			return;
		}
		if(c == ')') {
			if(!isFunctionCall(current)) {
				push(current);
				push(")");
				
				current = "";
				
				return;
			}else {
				push(current+")");
				
				current = "";
					
				return;
			}
		}
		
		
		current += c;
	}
	private void push(String text) {
		text = text.trim();
		
		if(text.isEmpty())
			return;
		
		System.err.println(text+" "+text.isEmpty()+" "+text.length());
		
		if(text.contains("mod")) {
			String first = text.substring(0, text.indexOf("mod"));
			String last = text.substring(text.indexOf("mod")+4);
			
			addToInstructions(new MinInstruction(first));
			addToInstructions(new MinInstruction("mod"));
			addToInstructions(new MinInstruction(last));
		}else {
			addToInstructions(new MinInstruction(text));
		}
	}
	
	
	private void addToInstructions(MinInstruction instruction) {
		if(instruction.isEmpty())
			throw new IllegalArgumentException("instruction is emtpy");
		
		instructions.add(instruction);
	}
	private void addToParent(MinInstruction instruction, MinInstruction parent) {
		if(instruction.isEmpty())
			throw new IllegalArgumentException("instruction is emtpy");
		
		parent.add(instruction);
	}
	private void addToInstructions(Collection<MinInstruction> instructions) {
		for(MinInstruction instruction : instructions) {
			addToInstructions(instruction);
		}
	}
	
	
	private boolean isFunctionCall(String text) {
		return ! text.startsWith("(") && text.contains("(") &&
				!(text.endsWith("*") || text.endsWith("/") || text.endsWith("+") || text.endsWith("-") 
				&& text.endsWith(")"));
	}
}
