package com.felixcool98.gml.objects;

import com.felixcool98.gml.Utils;

public class GMLObject {
	private Object object;
	private GMLObjectType type;
	
	
	public GMLObject() {
		
	}
	public GMLObject(Object object) {
		setValue(object);
	}
	
	
	public static GMLObject parseObject(String text) {
		if(text.startsWith("\""))
			return new GMLObject(text.substring(1, text.length()-1));
		else
			return new GMLObject(Utils.toNumber(text));
	}
	
	
	public GMLObjectType getType() {
		return type;
	}
	
	
	public GMLObject add(GMLObject object) {
		if(!getType().equals(object.getType()))
			throw new IllegalArgumentException("types of the objects are different");
		
		switch (getType()) {
			case REAL:
				if(getValue() instanceof Integer && object.getValue() instanceof Integer) {
					return new GMLObject((int) getValue() + (int) object.getValue());
				}else {
					float a = getValue() instanceof Float ? (float)getValue() : (float)(int)getValue();
					float b = object.getValue() instanceof Float ? (float) object.getValue() : (float)(int)object.getValue();
					
					return new GMLObject(a + b);
				}
			default:
				break;
		}
		
		return null;
	}
	public GMLObject subtract(GMLObject object) {
		if(!getType().equals(object.getType()))
			throw new IllegalArgumentException("types of the objects are different");
		
		switch (getType()) {
			case REAL:
				if(getValue() instanceof Integer && object.getValue() instanceof Integer) {
					return new GMLObject((int) getValue() - (int) object.getValue());
				}else {
					float a = getValue() instanceof Float ? (float)getValue() : (float)(int)getValue();
					float b = object.getValue() instanceof Float ? (float) object.getValue() : (float)(int)object.getValue();
					
					return new GMLObject(a - b);
				}
			default:
				break;
		}
		
		return null;
	}
	public GMLObject multiply(GMLObject object) {
		if(!getType().equals(object.getType()))
			throw new IllegalArgumentException("types of the objects are different");
		
		switch (getType()) {
			case REAL:
				if(getValue() instanceof Integer && object.getValue() instanceof Integer) {
					return new GMLObject((int) getValue() * (int) object.getValue());
				}else {
					float a = getValue() instanceof Float ? (float)getValue() : (float)(int)getValue();
					float b = object.getValue() instanceof Float ? (float) object.getValue() : (float)(int)object.getValue();
					
					return new GMLObject(a * b);
				}
			default:
				break;
		}
		
		return null;
	}
	public GMLObject divide(GMLObject object) {
		if(!getType().equals(object.getType()))
			throw new IllegalArgumentException("types of the objects are different");
		
		switch (getType()) {
			case REAL:
				if(getValue() instanceof Integer && object.getValue() instanceof Integer) {
					return new GMLObject((int) getValue() / (int) object.getValue());
				}else {
					float a = getValue() instanceof Float ? (float)getValue() : (float)(int)getValue();
					float b = object.getValue() instanceof Float ? (float) object.getValue() : (float)(int)object.getValue();
					
					return new GMLObject(a / b);
				}
			default:
				break;
		}
		
		return null;
	}
	public GMLObject intMod(GMLObject object) {
		if(!getType().equals(object.getType()))
			throw new IllegalArgumentException("types of the objects are different");
		
		switch (getType()) {
			case REAL:
				return new GMLObject((int) getValue() % (int) object.getValue());
				
			default:
				break;
		}
		
		return null;
	}
	public GMLObject intDiv(GMLObject object) {
		if(!getType().equals(object.getType()))
			throw new IllegalArgumentException("types of the objects are different");
		
		switch (getType()) {
			case REAL:
				int a = getValue() instanceof Integer ? (int)getValue() : (int)(float)getValue();
				int b = object.getValue() instanceof Integer ? (int) object.getValue() : (int)(float)object.getValue();
				
				return new GMLObject(a / b);
			default:
				break;
		}
		
		return null;
	}
	
	
	public void setValue(Object object) {
		this.object = object;
		
		if(object instanceof Float)
			type =  GMLObjectType.REAL;
		if(object instanceof Integer)
			type =  GMLObjectType.REAL;
		if(object instanceof String)
			type = GMLObjectType.STRING;
	}
	
	
	public Object getValue() {
		return object;
	}
	
	
	//======================================================================
	// checks
	//======================================================================
	public boolean valueEquals(Object other) {
		if(getValue() instanceof Float) {
			if(other instanceof Float) {
				return (float) getValue() == (float) other;
			}else if(other instanceof Integer) {
				return (float) getValue() == (int) other;
			}
		}else if(getValue() instanceof Integer) {
			if(other instanceof Float) {
				return (int) getValue() == (float) other;
			}else if(other instanceof Integer) {
				return (int) getValue() == (int) other;
			}
		}else if(getValue() instanceof String) {
			if(other instanceof String) {
				return getValue().equals(other);
			}
		}
		
		
		return false;
	}
	
	
	@Override
	public String toString() {
		return "GML Object : " + getValue().toString();
	}
}
