import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.gml.maths.parser.MinInstruction;

public class MinInstructionTests {

	@Test
	public void test() {
		MinInstruction instruction = new MinInstruction("mod");
		
		assertTrue(instruction.isSign());
		
		instruction = new MinInstruction();
		instruction.add(new MinInstruction("mod"));
		instruction.fixChildren();
		
		assertTrue(instruction.isSign());
	}

}
