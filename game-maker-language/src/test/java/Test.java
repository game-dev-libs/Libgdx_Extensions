
import com.felixcool98.gml.Instruction;
import com.felixcool98.gml.InstructionParser;
import com.felixcool98.gml.scripts.ActionScriptBuilder;
import com.felixcool98.gml.scripts.Script;

public class Test {

	@org.junit.Test
	public void test() {
		String text = "/// draw_background_tiled_area(background,x,y,x1,y2,x2,y2)\r" + 
				"//\r" + 
				"//  Draws a repeated background image, tiled to fill a given region and with\r" + 
				"//  a given offset. \r" + 
				"//\r" + 
				"//      background  background to be drawn\r" + 
				"//      x,y         origin offset, real\r" + 
				"//      x1,y1       top-left corner of tiled area, real\r" + 
				"//      x2,y2       bottom-right corner of tiled area, real\r" + 
				"//\r" + 
				"/// GMLscripts.com/license\r" + 
				"{\r" + 
				"    var bg,xx,yy,x1,y1,x2,y2;\r" + 
				"    bg = argument0;\r" + 
				"    xx = argument1;\r" + 
				"    yy = argument2;\r" + 
				"    x1 = argument3;\r" + 
				"    y1 = argument4;\r" + 
				"    x2 = argument5;\r" + 
				"    y2 = argument6;\r" + 
				"\r" + 
				"    var bw,bh,i,j,jj,left,top,width,height,X,Y;\r" + 
				"    bw = background_get_width(bg);\r" + 
				"    bh = background_get_height(bg);\r" + 
				"\r" + 
				"    //i = x1-((x1 mod bw) - (xx mod bw)) - bw*((x1 mod bw)<(xx mod bw));\r" + 
				"    //j = y1-((y1 mod bh) - (yy mod bh)) - bh*((y1 mod bh)<(yy mod bh)); \r" + 
				"    jj = j;\r" + 
				"\r" + 
				"    for(i=i; i<=x2; i+=bw) {\r" + 
				"        for(j=j; j<=y2; j+=bh) {\r" + 
				"\r" + 
				"            if(i <= x1) left = x1-i;\r" + 
				"            else left = 0;\r" + 
				"            X = i+left;\r" + 
				"\r" + 
				"            if(j <= y1) top = y1-j;\r" + 
				"            else top = 0;\r" + 
				"            Y = j+top;\r" + 
				"\r" + 
				"            if(x2 <= i+bw) width = ((bw)-(i+bw-x2)+1)-left;\r" + 
				"            else width = bw-left;\r" + 
				"\r" + 
				"            if(y2 <= j+bh) height = ((bh)-(j+bh-y2)+1)-top;\r" + 
				"            else height = bh-top;\r" + 
				"\r" + 
				"            draw_background_part(bg,left,top,width,height,X,Y);\r" + 
				"        }\r" + 
				"        j = jj;\r" + 
				"    }\r" + 
				"    return 0;\r" + 
				"}";
		
		Instruction[] instructions = new InstructionParser().parse(text);
		for(Instruction instruction : instructions) {
			System.out.println(instruction.toString());
		}
		Script script = new ActionScriptBuilder().build(instructions);
		System.out.println(script.toString());
		
		
		//Scopes
//		Scope globalScope = new Scope();
//		Scope objectScope = new Scope();
		
//		globalScope.add("background_get_width", new JavaScript() {
//			@Override
//			public GMLObject execute(Scope global, Scope object, GMLObject... objects) {
//				System.out.println(objects.length);
//				
//				return new GMLObject(100);
//			}
//		});
//		globalScope.add("background_get_height", new JavaScript() {
//			@Override
//			public GMLObject execute(Scope global, Scope object, GMLObject... objects) {
//				System.out.println(objects.length);
//				
//				return new GMLObject(100);
//			}
//		});
//		
//		System.out.println(objectScope.toString());
//		
//		script.execute(globalScope, objectScope, 0, 0, 0, 0, 0, 0, 0);
//		
//		System.out.println(objectScope.toString());
	}

}
